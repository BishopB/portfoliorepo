<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html"  pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!--Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel ="stylesheet">
        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">
        <!-- SWC icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
        <style>
            .theme1 {
                background-color: ${rct[0]};
            }
            .theme2 {
                background-color: ${rct[1]};
            }
            .white {
                background-color: white;
            }
            .btn-panel {
                width: 300px;
                height: 400px;
                vertical-align: middle;
                display: table-cell;
            }
            .btn-batcave {
                width: 128px;
                color: whitesmoke;
                background-size: 128px 32px;
                background-image: url(../img/BatLogo.png);
            }
            .btn-batcave:hover {
                font-weight: bold;
                color: lightgoldenrodyellow;
            }
        </style>
    </head>

    <body class="theme2">
        <div class ="container theme1">
            <div class="row">
                <div class ="col-md-6 col-md-push-3">
                    <h1>Flooring Mastery 9001 <small>Admin - Batcave</small></h1>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-4 col-md-push-1">
                    <a href="${pageContext.request.contextPath}/admin/products" class="btn btn-default btn-panel">Products</a>
                </div>
                <div class="col-md-4 col-md-push-3">
                    <a href="${pageContext.request.contextPath}/admin/states" class="btn btn-default btn-panel">States</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-push-2 text-center">
                    <a href="${pageContext.request.contextPath}/" class="btn btn-default btn-batcave">Leave Batcave</a>
                </div>
            </div>
        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.0.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>
