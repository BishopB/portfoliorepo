<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html"  pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!--Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel ="stylesheet">
        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">
        <!-- SWC icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
        <style>
            h2{
                text-align: center;
            }

            .theme1 {
                background-color: ${rct[0]};
            }
            .theme2 {
                background-color: ${rct[1]};
            }
            .white {
                background-color: white;
            }
            .errorMsg {
                color: red;
                text-align: left;
                font-style: italic;
            }
        </style>
    </head>

    <body class="theme2">
        <div class ="container theme1">
            <div class="row">
                <div class ="col-md-5 col-md-push-1">
                    <h2>Flooring MasterY 9001</h2>
                    <table id="orderTable" class="table white">
                        <tr>
                            <th width="60%">Order Info</th>
                            <th width="20%"></th>
                            <th width="20%"></th>
                        </tr>
                        <c:forEach items="${allTheOrders}" var="order">
                            <tr id = 'order-row-${order.id}'>
                                <td><a data-order-id="${order.id}" data-toggle="modal" data-target="#showOrderModal">${order.fileName.substring(7,11)}/${order.fileName.substring(12,14)}/${order.fileName.substring(15,17)} : ${order.orderNum}, ${order.name} </a></td>
                                <td><a data-order-id="${order.id}" data-toggle="modal" data-target="#editOrderModal">Edit</a></td>
                                <td><a data-order-id="${order.id}" class = "delete-link">Delete</a></td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class ="col-md-4 col-md-push-1">

                    <span style="text-align:center;"><h2>${fn: contains(requestScope['javax.servlet.forward.request_uri'],"showEdit")?"Edit Order" : "Add New Order" }</h2></span>

                    <form id="create-order-form"class ="form-horizontal">
                        <!--name-->
                        <div class="form-group">
                            <label for="add-name" class="col-md-4 control-label">Customer Name:</label>
                            <div class="col-md-8">
                                <div id="add-order-validation-errors-name" class="errorMsg"></div>
                                <input type="text" name="name" class="form-control" id="add-name" placeholder="Customer Name" />
                            </div>
                        </div>
                        <!--product-->
                        <div class="form-group">

                            <label for="add-productId" class="col-md-4 control-label">Product:</label>
                            <div class="col-md-8 selectContainer">
                                <div id="add-order-validation-errors-productId" class="errorMsg"></div>
                                <select id = "add-productId" name="productType" class="form-control">
                                    <option value=''>Please Enter A Product</option>
                                    <c:forEach items="${allTheProducts}" var ="product">
                                        <option value="${product.id}">${product.productType}</option>
                                    </c:forEach>
                                    <form:errors path="productType" />
                                </select>
                            </div>
                        </div>
                        <!--state-->
                        <div class="form-group">

                            <label for="add-stateId" class="col-md-4 control-label">State:</label>
                            <div class="col-md-8 selectContainer">
                                <div id="add-order-validation-errors-stateId" class="errorMsg"></div>
                                <select id = "add-stateId" name="state" class="form-control">
                                    <option value=''>Please Enter A State</option>

                                    <c:forEach items="${allTheStates}" var ="state">
                                        <option value="${state.id}">${state.state}</option>
                                    </c:forEach>
                                    <form:errors path="state" />
                                </select>
                            </div>
                        </div>
                        <!--area-->
                        <div class="form-group">
                            <label for="add-area" class="col-md-4 control-label">Area:</label>
                            <div class="col-md-8">
                                <div id="add-order-validation-errors-area" class="errorMsg"></div>
                                <input type="text" name="area" class="form-control" id="add-area" placeholder="Area" />

                            </div>
                        </div>
                        <!--filename/date-->
                        <div class="form-group">
                            <label for="add-date" class="col-md-4 control-label">Date:</label>
                            <div class="col-md-8">
                                <div id="add-order-validation-errors-date" class="errorMsg"></div>
                                <input type="text" name="fileName" class="form-control" id="add-date" placeholder="Date" />
                            </div>
                        </div>

                        <input type="submit" class="btn btn-default pull-right" value="Create"/>
                    </form>
                </div>
            </div>
            <br />
            <br />
            <br />

            <div class="search-area col-md-6 col-md-push-3">
                <form id="search-order-form" class="form-horizontal">
                    <!-- search -->
                    <div class="input-group">
                        <input id="searchValue" type="text" class="form-control" name="searchValue" placeholder="Search for..." value="${searchValue.length()>0 ? searchValue : ''}">
                        <span class="input-group-btn">
                            <button id="search-order-btn" class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                        </span>
                    </div>
                </form>
            </div>
        </div>

        <div id="showOrderModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Order Details</h4>
                    </div>
                    <div class="modal-body">

                        <table class="table table-bordered">
                            <tr>
                                <th>Date:</th>
                                <td id="order-date"></td>
                            </tr>
                            <tr>
                                <th>Customer Name:</th>
                                <td id="order-name"></td>
                            </tr>
                            <tr>
                                <th>Product:</th>
                                <td id="order-product"></td>
                            </tr>
                            <tr>
                                <th>Product Cost Per Sq Ft:</th>
                                <td id="order-product-cost-per-sq-ft"></td>
                            </tr>
                            <tr>
                                <th>Labor Cost Per Sq Ft:</th>
                                <td id="order-labor-cost-per-sq-ft"></td>
                            </tr>
                            <tr>
                                <th>State:</th>
                                <td id="order-state"></td>
                            </tr>
                            <tr>
                                <th>Tax Rate:</th>
                                <td id="order-tax-rate"></td>
                            </tr>
                            <tr>
                                <th>Area:</th>
                                <td id="order-area"></td>
                            </tr>
                            <tr>
                                <th>Material Cost:</th>
                                <td id="order-material-cost"></td>
                            </tr>
                            <tr>
                                <th>Labor Cost:</th>
                                <td id="order-labor-cost"></td>
                            </tr>
                            <tr>
                                <th>Tax Total:</th>
                                <td id="order-tax-total"></td>
                            </tr>
                            <tr>
                                <th>Total Cost:</th>
                                <td id="order-total-cost"></td>
                            </tr>
                        </table>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        <div id="editOrderModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Order</h4>
                    </div>
                    <div class="modal-body">

                        <table class="table table-bordered">
                            <tr>
                                <th>Date:</th>
                                <td>
                                    <div id="edit-order-validation-errors-date" class="errorMsg"></div>
                                    <input type="text" id = "edit-date" />
                                </td>
                            </tr>
                            <tr>
                                <th>Customer Name:</th>
                                <td>
                                    <div id="edit-order-validation-errors-name" class="errorMsg"></div>
                                    <input type="text" id = "edit-name" />
                                </td>
                            </tr>
                            <tr>
                                <th>Product:</th>
                                <td>
                                    <select id = "edit-productId" name="productType" class="form-control">
                                        <c:forEach items="${allTheProducts}" var ="product">
                                            <option value="${product.id}">${product.productType}</option>
                                        </c:forEach>
                                        <form:errors path="productType" />
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>State:</th>
                                <td>
                                    <select id = "edit-stateId" name="state" class="form-control">
                                        <c:forEach items="${allTheStates}" var ="state">
                                            <option value="${state.id}">${state.state}</option>
                                        </c:forEach>
                                        <form:errors path="state" />
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>Area:</th>
                                <td>
                                    <div id="edit-order-validation-errors-area" class="errorMsg"></div>
                                    <input type="text" id = "edit-area" />
                                </td>
                            </tr>
                        </table>

                        <button  data-contact-id=""  id="edit-order-button" class ="btn btn-default" value ="Edit Contact">Edit Contact</button>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        <script type="text/javascript">
            var contextRoot = '${pageContext.request.contextPath}';
        </script>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/userApp.js"></script>
    </body>
</html>
