$(document).ready(function () {

    $(document).on('submit', '#create-order-form', function (e) {
        console.log("submit form");
        e.preventDefault();
        $('.errorMsg').empty();
        var orderData = JSON.stringify({
            name: $('#add-name').val(),
            productId: $('#add-productId').val(),
            stateId: $('#add-stateId').val(),
            area: $('#add-area').val(),
            date: $('#add-date').val(),
            fileName: "Orders_" + $('#add-date').val() + ".txt"
        });
        $.ajax({
            type: 'POST',
            url: contextRoot + '/user',
            data: orderData,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            }
        }).success(function (data, status) {
            $('#add-first-name').val("");
            $('#add-productId').val("");
            $('#add-stateId').val("");
            $('#add-area').val("");
            $('#add-date').val("");

            var tableRow = buildOrderRow(data);
            $('#orderTable').append($(tableRow));
        }).error(function (data, status) {
            var errors = data.responseJSON.errors;

            $.each(errors, function (index, validationError) {
                $('#add-order-validation-errors-' + validationError.fieldName)
                        .append("* " + validationError.message)
                        .append("<br />");
            });
        });
    });

    $('#showOrderModal').on('show.bs.modal', function (e) {
        var link = $(e.relatedTarget);
        var orderId = link.data('order-id');
        var modal = $(this);

        $.ajax({
            type: 'GET',
            url: contextRoot + '/user/' + orderId,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Accept', 'application/json');
            }
        }).success(function (order, status) {
            console.log(order);
            console.log(status);
//            for(var key in order){
//                modal.find("#order-" + key).text()
//            }
            $.each(order, function (key, value) {
                console.log(key);
                modal.find("#order-" + key).text(value);
            });
        });
    });

    $('#editOrderModal').on('show.bs.modal', function (e) {
        var link = $(e.relatedTarget);
        var orderId = link.data('order-id');
        var modal = $(this);

        $.ajax({
            type: 'GET',
            url: contextRoot + '/user/' + orderId,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Accept', 'application/json');
            }
        }).success(function (order, status) {
            $('#edit-productId').val(order.productId);
            $('#edit-stateId').val(order.stateId);
            $.each(order, function (key, value) {
                console.log(key);
                modal.find("#edit-" + key).val(value);
            });
        });
    });

    $(document).on('click', '.delete-link', function (e) {
        e.preventDefault();
        var orderId = $(e.target).data('order-id');
        $.ajax({
            type: 'DELETE',
            url: contextRoot + '/user/' + orderId
        }).success(function (data, status) {
            $('#order-row-' + orderId).remove();
        }).error(function (data, status) {
            var errors = data.responseJSON.errors;

            $.each(errors, function (index, validationError) {
                $('#edit-order-validation-errors-' + validationError.fieldName)
                        .append("* " + validationError.message)
                        .append("<br />");
            });
        });

        $(document).on('click', '#edit-order-button', function (e) {



        });


        function buildOrderRow(order) {
            return"<tr>\n\
             <td><a data-order-id='" + order.id + "' data-toggle='modal' data-target='#showOrderModal'>" + order.fileName.substring(7, 11) + "/" + order.fileName.substring(12, 14) + "/" + order.fileName.substring(15, 17) + " : " + order.orderNum + ", " + order.name + "</a></td> \n\
             <td><a data-order-id='" + order.id + "' data-toggel='modal' data-target='#editContactModal'>Edit</a></td> \n\
             <td><a data-order-id='" + order.id + "' class = 'delete-link'>Delete</a></td>\n\
             </tr>";

        }

    });

    $(document).on('click', '#search-order-btn', function (e) {
        e.preventDefault();

                //Definitely most certainly not the batcave entrance
        if ($('#searchValue').val()==="batcave") {
            window.location.href = contextRoot+'/admin/choose';
        }

        $.ajax({
            type: 'GET',
            url: contextRoot + '/user/search/' + $('#searchValue').val(),
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Accept', 'application/json');
            }
        }).success(function (data, status) {
            //clear table except first row with headers
            $('#orderTable').find("tr:gt(0)").remove();

            //print results
            $.each(data, function (index, Order) {
                $('#orderTable').append(buildOrderRow(Order));
            });

            //No results
            if ($(data).size() === 0) {
                $('#orderTable').append(buildNoResultsRow());
            }
        });

        function buildNoResultsRow() {
            return "<tr id='no-result-row' class='emptyResults text-center'>  \n\
                    <td colspan='3'><h3><small>No Results</small></h3></td> \n\
                </tr>";
        }
        
        function buildOrderRow(order) {
            return '<tr id = "order-row-'+order.id+'"> \n\
                                <td><a data-order-id="'+order.id+'" data-toggle="modal" data-target="#showOrderModal">'+order.fileName.substring(7,11)+'/'+order.fileName.substring(12,14)+'/'+order.fileName.substring(15,17)+' : '+order.orderNum+', '+order.name+' </a></td>   \n\
                                <td><a data-order-id="'+order.id+'" data-toggle="modal" data-target="#editOrderModal">Edit</a></td>   \n\
                                <td><a data-order-id="'+order.id+'" class = "delete-link">Delete</a></td>   \n\
                            </tr>';
        }
    });//END search orders
});

