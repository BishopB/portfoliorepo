/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.flooringmaster9001.encodeDecode;

import com.thesoftwareguild.flooringmaster9001.models.Product;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class EncodeDecodeProduct {

    public List<Product> decode(String FILENAME, String DELIMETER) {
        List<Product> productList = new ArrayList<>();
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(FILENAME)));
            //implemented to skip over the header line in text file
            sc.nextLine();
            while (sc.hasNextLine()) {
                String currentLine = sc.nextLine();
                String[] values = currentLine.split(DELIMETER);
                productList.add(new Product(Integer.parseInt(values[3]),values[0], Double.parseDouble(values[1]), Double.parseDouble(values[2])));
            }
            Collections.sort(productList, (Product o1, Product o2) -> o1.getProductType().compareToIgnoreCase(o2.getProductType()));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EncodeDecodeProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return productList;
    }

    public void encode(boolean testMode, List<Product> productList, String FILENAME, String DELIMETER) {
        if (!testMode) {
            try {
                PrintWriter pw = new PrintWriter(new FileWriter(FILENAME));
                pw.println("ProductType, CostPerSquareFoot, LaborCostPerSquareFoot");
                for (Product currentProduct : productList) {
                    String line
                            = currentProduct.getProductType() + DELIMETER
                            + currentProduct.getCostSqFt() + DELIMETER
                            + currentProduct.getLaborCostSqFt() + DELIMETER
                            + currentProduct.getId();

                    pw.println(line);
                    pw.flush();

                }
                pw.close();
            } catch (IOException ex) {
                Logger.getLogger(EncodeDecodeProduct.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
