/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.flooringmaster9001.controller;

import com.thesoftwareguild.flooringmaster9001.dao.ConfigDao;
import com.thesoftwareguild.flooringmaster9001.dao.OrderDao;
import com.thesoftwareguild.flooringmaster9001.dao.ProductDao;
import com.thesoftwareguild.flooringmaster9001.dao.StatesDao;
import com.thesoftwareguild.flooringmaster9001.models.Order;
import com.thesoftwareguild.flooringmaster9001.models.Product;
import com.thesoftwareguild.flooringmaster9001.models.States;
import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping("/user")
public class UserController {

    private ConfigDao configDao;
    private OrderDao orderDao;
    private ProductDao productDao;
    private StatesDao statesDao;
    private RCT_Generator rct;

    @Inject
    public UserController(ConfigDao cDao, OrderDao oDao,
            ProductDao pDao, StatesDao tDao, RCT_Generator rctBean) {
        this.configDao = cDao;
        this.orderDao = oDao;
        this.productDao = pDao;
        this.statesDao = tDao;
        this.rct = rctBean;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)//Value doesn't Change
    @ResponseBody
    public Order create(@Valid @RequestBody Order order) {

        //grabs the laborCost from productDao with order.productId
        order.setLaborCost(order.getArea() * productDao.getProduct(order.getProductId()).getLaborCostSqFt());

        order.setMaterialCost(order.getArea()
                * productDao.getProduct(order.getProductId())//get the product
                .getCostSqFt());//get the cost per sq ft.

        order.setTaxCost((order.getLaborCost()
                + order.getMaterialCost())
                * (statesDao.getTax(order.getStateId()) / 100));//stateDao gets the Tax with the order State Id, good method Kyle.

        order.setTotalCost(order.getTaxCost()
                + order.getLaborCost()
                + order.getMaterialCost());

        Order addedOrder = orderDao.createOrder(order);
        return addedOrder;
    }

//    @RequestMapping(value = "/{fileName}({orderNum})")//Change This Value, index as well
    @RequestMapping(value = "/{id}", produces = "application/json")
    @ResponseBody
    public String showOrder(@PathVariable("id") Integer orderId) {
        //method body
//        List<Object> response = new ArrayList();
        Order order = orderDao.getOrder(orderId);
        Product product = productDao.getProduct(order.getProductId());
        States state = statesDao.getState(order.getStateId());

//        String response = order.toString()+ product.toString()+state.toString();
        JsonObject response = Json.createObjectBuilder()
                .add("date", order.getDate().toString())
                .add("name", order.getName())
                .add("product", product.getProductType())
                .add("product-cost-per-sq-ft", product.getCostSqFt())
                .add("labor-cost-per-sq-ft", product.getLaborCostSqFt())
                .add("state", state.getState())
                .add("tax-rate", state.getTaxRate())
                .add("area", order.getArea())
                .add("material-cost", order.getMaterialCost())
                .add("labor-cost", order.getLaborCost())
                .add("tax-total", order.getTaxCost())
                .add("total-cost", order.getTotalCost())
                .add("productId", order.getProductId())
                .add("stateId", order.getStateId())
                .build();

        return response.toString();

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)//Change This Value, at the index too.
    @ResponseBody
    public void delete(@PathVariable("id") Integer orderNum) {
        Order currentOrder = orderDao.getOrder(orderNum);
        orderDao.deleteOrder(currentOrder);

    }

    //LEGACY
    @RequestMapping(value = "/{fileName}({orderNum})", method = RequestMethod.GET)//Change Value, index as well.
    @ResponseBody
    public Order search(@PathVariable("orderNum") Integer orderNum,
            @PathVariable("fileName") String fileName) {

        return orderDao.getOrder(orderNum, fileName);
    }

    @RequestMapping(value = "/{id})", method = RequestMethod.PUT)//Vhange Value, index as well
    @ResponseBody
    public Order edit(@Valid @RequestBody Order order, BindingResult bindingResult,
            @PathVariable("id") Integer orderId) {//method body

        //grabs the laborCost from productDao with order.productId
        order.setLaborCost(order.getArea() * productDao.getProduct(order.getProductId()).getLaborCostSqFt());

        order.setMaterialCost(order.getArea()
                * productDao.getProduct(order.getProductId())//get the product
                .getCostSqFt());//get the cost per sq ft.

        order.setTaxCost((order.getLaborCost()
                + order.getMaterialCost())
                * (statesDao.getTax(order.getStateId()) / 100));//stateDao gets the Tax with the order State Id, good method Kyle.

        order.setTotalCost(order.getTaxCost()
                + order.getLaborCost()
                + order.getMaterialCost());

        orderDao.updateOrder(order);

        return order;
    }

    @RequestMapping(value = "/search/{searchValue}")
    @ResponseBody
    public List<Order> searchProduct(@PathVariable("searchValue") String searchValue) {
        searchValue += "";
        return searchOrders(searchValue);
    }

    private List<Order> searchOrders(String searchValue) {
        return orderDao.list()
                .stream()
                .filter(x-> x.getName().toLowerCase().contains(searchValue.toLowerCase())
                    ||  x.getFileName().toLowerCase().contains(searchValue.toLowerCase())
                    ||  productDao.getProduct(x.getProductId()).getProductType().toLowerCase().contains(searchValue.toLowerCase())
                    ||  statesDao.getState(x.getStateId()).getState().toLowerCase().contains(searchValue.toLowerCase())
                    ||  (x.getArea()+"").contains(searchValue)
                    ||  (x.getMaterialCost()+"").contains(searchValue)
                    ||  (x.getLaborCost()+"").contains(searchValue)
                    ||  (x.getTaxCost()+"").contains(searchValue)
                    ||  (x.getTotalCost()+"").contains(searchValue)
                )
                .collect(Collectors.toList());
    }
}
