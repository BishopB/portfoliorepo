/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.dao;

import com.thesoftwareguild.akronautobodycms.dto.Account;
import com.thesoftwareguild.akronautobodycms.viewModel.AccountEditObject;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface AccountInf extends CRUD<Account>{
    
    public Account create (Account account);
    
    public Account read (Integer id);
    
    public Account update (Account account);
    
    public void delete(Integer id); 
    
    public List <Account> list();
    
    public Account readByName(String userName);

    public boolean isFree(Integer id);
    
    public boolean editUnique(AccountEditObject o);
    
}
