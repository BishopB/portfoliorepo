/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.dao;

import com.thesoftwareguild.akronautobodycms.dto.Quote;
import com.thesoftwareguild.akronautobodycms.dto.QuoteModalInfo;
import com.thesoftwareguild.akronautobodycms.dto.UploadFile;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface QuoteInf extends CRUD<Quote> {
    
    public void sendMsg(Quote quote, String status);
    
    public UploadFile get(Integer id);

    public Quote create(Quote quote);

    public Quote read(Integer id);

    public Quote update(Quote quote);

    public void delete(Integer id);

    public List<Quote> list();

    public QuoteModalInfo getQuoteModalOptions(QuoteModalInfo info);

    public QuoteModalInfo getYears();

    public QuoteModalInfo getMakes(QuoteModalInfo info);

    public QuoteModalInfo getModels(QuoteModalInfo info);

}
