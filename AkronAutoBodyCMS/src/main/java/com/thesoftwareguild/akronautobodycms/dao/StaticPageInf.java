/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.dao;

import com.thesoftwareguild.akronautobodycms.dto.StaticPage;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface StaticPageInf extends CRUD<StaticPage>{
    
    public StaticPage create (StaticPage staticPage);
    
    public StaticPage read (Integer id);
    
    public StaticPage update (StaticPage staticPage);
    
    public void delete(Integer id); 
    
    public List <StaticPage> list();
    
}
