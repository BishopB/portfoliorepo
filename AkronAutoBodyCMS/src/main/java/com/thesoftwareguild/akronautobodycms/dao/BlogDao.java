/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.dao;

import com.thesoftwareguild.akronautobodycms.dto.Account;
import com.thesoftwareguild.akronautobodycms.dto.Blog;
import com.thesoftwareguild.akronautobodycms.dto.Category;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class BlogDao implements BlogDaoInf {

    private static final String SQL_INSERT_BLOG = "INSERT INTO `blog` (`title`, `startDate`, `endDate`, `account_id`, `category_id`, `content`, `anchor`,`approved`)VALUES(?,?,?,?,?,?,?,?)";
    private static final String SQL_GET_BLOG = "SELECT * FROM blog WHERE blog_id=?";
    private static final String SQL_UPDATE_BLOG = "UPDATE blog SET title=?, startDate=?, endDate=?, account_id=?, category_id=?, content=?, anchor=?, approved=? WHERE blog_id= ?";
    private static final String SQL_DELETE_BLOG = "DELETE FROM blog WHERE blog_id=?";
    private static final String SQL_LIST_BLOGS = "SELECT * FROM blog WHERE endDate > CURDATE() AND approved>0 ORDER BY startDate DESC";

    private static final String SQL_LIST_BY_HASHTAG = "SELECT * FROM blog WHERE content LIKE (CONCAT('%',?,'%')) AND endDate > CURDATE() AND approved>0  ORDER BY startDate DESC";
    private static final String SQL_LIST_BY_CATEGORY = "SELECT * FROM blog b INNER JOIN category c ON c.category_id = b.category_id WHERE c.name LIKE CONCAT('%',?,'%') AND endDate > CURDATE() AND approved>0  ORDER BY startDate DESC";            
    private static final String SQL_LIST_BY_AUTHOR = "SELECT * FROM blog INNER JOIN account ON account.account_id = blog.account_id WHERE UPPER (account.name) LIKE UPPER (CONCAT('%',?,'%')) AND endDate > CURDATE() AND approved>0  ORDER BY startDate DESC";
    private static final String SQL_LIST_BY_KEYWORD = "SELECT * FROM blog WHERE content LIKE (CONCAT('%',?,'%')) OR title LIKE (CONCAT('%',?,'%')) AND endDate > CURDATE() AND approved>0 ORDER BY startDate DESC";
    private static final String SQL_LIST_ALL_BLOGS = "SELECT * FROM blog";
    
    
    
    JdbcTemplate template;
    private Boolean isInProduction;

    public BlogDao(JdbcTemplate template) {
        this.template = template;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Blog create(Blog blog) {
        
            LocalDate startDate =blog.getStartDate();
            LocalDate endDate = blog.getEndDate();            
            
            template.update(SQL_INSERT_BLOG,
                    blog.getTitle(),
                    Date.from(startDate.atStartOfDay(ZoneId.systemDefault()).toInstant()),
                    Date.from(endDate.atStartOfDay(ZoneId.systemDefault()).toInstant()),
                    blog.getAccount().getId(),
                    blog.getCategory().getId(),
                    blog.getContent(),
                    blog.getAnchor(),
                    blog.getApproved()
            );
            int newId = template.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
            blog.setId(newId);

            return blog;
        

    }

    @Override
    public Blog read(Integer id) {
        Blog blog = template.queryForObject(SQL_GET_BLOG, new BlogMapper(), id);
        return blog;
    }

    @Override
    public Blog update(Blog blog) {
        try {
            LocalDate startDate =blog.getStartDate();
            LocalDate endDate = blog.getEndDate();
            
            template.update(SQL_UPDATE_BLOG,
                    blog.getTitle(),
                    Date.from(startDate.atStartOfDay(ZoneId.systemDefault()).toInstant()),
                    Date.from(endDate.atStartOfDay(ZoneId.systemDefault()).toInstant()),
                    blog.getAccount().getId(),
                    blog.getCategory().getId(),
                    blog.getContent(),
                    blog.getAnchor(),
                    blog.getApproved(),
                    blog.getId()
            );

            return blog;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void delete(Integer id) {
        template.update(SQL_DELETE_BLOG, id);
    }

    @Override
    public List<Blog> list() {
        List<Blog> list = template.query(SQL_LIST_BLOGS, new BlogMapper());
        return list;
    }
    
    @Override
    public List<Blog> listAllBlogs() {
        List<Blog> listAll = template.query(SQL_LIST_ALL_BLOGS, new BlogMapper());
        return listAll;
    }

    @Override
    public List<Blog> listByHashTag(String hashtag) {
        List<Blog> listByHashTag = template.query(SQL_LIST_BY_HASHTAG, new BlogMapper(), hashtag);
        return listByHashTag;
    }

    //works in workbench but throws grammar exception in java, WHY???
    @Override
    public List<Blog> listByCategory(String category) {
        List<Blog> listByCategory = template.query(SQL_LIST_BY_CATEGORY, new BlogMapper(), category);
        return listByCategory;
    }

    @Override
    public List<Blog> listByAuthor(String author) {
        List<Blog> listByAuthor = template.query(SQL_LIST_BY_AUTHOR, new BlogMapper(), author);
        return listByAuthor;
    }

    @Override
    public List<Blog> listByKeyword(String keyword) {
        List<Blog> listByKeyWord = template.query(SQL_LIST_BY_KEYWORD, new BlogMapper(), keyword, keyword);
        return listByKeyWord;
    }

    /**
     * @param isInProduction the isInProduction to set
     */
    public void setIsInProduction(Boolean isInProduction) {
        this.isInProduction = isInProduction;
    }

    private static class BlogMapper implements RowMapper<Blog> {

        @Override
        public Blog mapRow(ResultSet rs, int i) throws SQLException {
            Blog blog = new Blog();
            Account account = new Account();
            Category category = new Category();
            account.setId(rs.getInt("account_id"));
            category.setId(rs.getInt("category_id"));

            blog.setId(rs.getInt("blog_id"));
            Date startDate= rs.getTimestamp("startDate");
            blog.setStartDate(startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
            Date endDate = rs.getTimestamp("endDate");
            blog.setEndDate(endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
            blog.setTitle(rs.getString("title"));
            blog.setAccount(account);
            blog.setCategory(category);
            blog.setContent(rs.getString("content"));
            blog.setAnchor(rs.getString("anchor"));
            blog.setApproved(rs.getBoolean("approved"));
            

            return blog;
        }
    }

}
