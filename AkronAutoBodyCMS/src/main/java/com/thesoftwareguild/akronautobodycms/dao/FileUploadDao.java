/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.dao;

import com.thesoftwareguild.akronautobodycms.dto.UploadFile;
import com.thesoftwareguild.akronautobodycms.dto.UploadItem;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class FileUploadDao {
    /*
        getFileList() and UploadItem are only used to display a list of files, but not core to functionality
    */

    private JdbcTemplate template;
    private static final String SQL_INSERT_UPLOAD = "INSERT INTO file_uploads (file_name, file_type, file_data) VALUES (?, ?, ?);";
    private static final String SQL_GET_UPLOAD = "SELECT * FROM file_uploads WHERE id = ?;";

    private static final String SQL_LIST_ITEMS = "SELECT file_name, id FROM file_uploads;";
    
    public FileUploadDao(JdbcTemplate templateBean) {
        this.template = templateBean;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public UploadFile add(UploadFile uploadFile) {
        template.update(SQL_INSERT_UPLOAD, uploadFile.getFileName(), uploadFile.getFileType(), uploadFile.getData());

        int newInt = template.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

        uploadFile.setId(newInt);

        return uploadFile;
    }

    public UploadFile get(Integer id) {
        UploadFile uploadFile = template.queryForObject(SQL_GET_UPLOAD, new UploadFileMapper(), id);

        return uploadFile;
    }
    
    public List<UploadItem> getFileList(){
        List<UploadItem> fileList = template.query(SQL_LIST_ITEMS, new UploadItemMapper());
        
        return fileList;
    }

    private static class UploadFileMapper implements RowMapper<UploadFile> {

        public UploadFileMapper() {
        }

        @Override
        public UploadFile mapRow(ResultSet rs, int i) throws SQLException {
            UploadFile file = new UploadFile();

            file.setId(rs.getLong("id"));
            file.setFileName(rs.getString("file_name"));
            file.setFileType(rs.getString("file_type"));
            file.setData(rs.getBytes("file_data"));

            return file;
        }
    }
        private static class UploadItemMapper implements RowMapper<UploadItem> {

        public UploadItemMapper() {
        }

        @Override
        public UploadItem mapRow(ResultSet rs, int i) throws SQLException {
            UploadItem item = new UploadItem();

            item.setId(rs.getLong("id"));
            item.setName(rs.getString("file_name"));

            return item;
        }
    }

}
