/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.dao;

import com.thesoftwareguild.akronautobodycms.dto.Account;
import com.thesoftwareguild.akronautobodycms.viewModel.AccountEditObject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class AccountDao implements AccountInf {

    private static final String SQL_INSERT_ACCOUNT = "INSERT INTO account ( `name`, `email`, `password`, `enabled`) VALUES (?,?,?,?)";
    private static final String SQL_GET_ACCOUNT = "SELECT * FROM account WHERE account_id =?";
    private static final String SQL_GET_ACCOUNT_BY_NAME = "SELECT * FROM account WHERE email =?";

    private static final String SQL_UPDATE_ACCOUNT = "UPDATE account SET `name`=?,`email`=?, `password`=?, `enabled`=? WHERE `account_id`=?";
    private static final String SQL_DELETE_ACCOUNT = "DELETE FROM  account WHERE account_id = ?";
    private static final String SQL_LIST_ACCOUNTS = "SELECT * FROM account";
    private static final String SQL_INSERT_AUTHORITIES = "insert into authorities(user_id, authority) values(?,?)";
    private static final String SQL_UPDATE_AUTHORITIES = "";
    private static final String SQL_DELETE_AUTHORITIES = "delete from authorities where user_id = ?";
    private static final String SQL_GET_AUTHORITIES_LIST = "SELECT authority from authorities WHERE user_id=? ";

    private static final String SQL_COUNT_DEPENDENCIES = "SELECT COUNT(*) FROM blog WHERE account_id = ?;";

    //can be used for isEmailUnique
    private static final String SQL_LIST_ACCOUNTS_BY_EMAIL = "SELECT COUNT(*) FROM account WHERE account_id != ? AND email = ?;";

    private JdbcTemplate template;

    public AccountDao(JdbcTemplate template) {
        this.template = template;
    }

    @Override
    public boolean isFree(Integer id) {
        return (!(template.queryForObject(SQL_COUNT_DEPENDENCIES, Integer.class, id)>=1));
    }
    
        @Override
    public boolean editUnique(AccountEditObject o) {
        return (!(template.queryForObject(SQL_LIST_ACCOUNTS_BY_EMAIL, Integer.class, o.getId(), o.getEmail())>=1));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Account create(Account account) {
        template.update(SQL_INSERT_ACCOUNT,
                account.getName(),
                account.getEmail(),
                account.getPassword(),
                account.isEnabled());
        int newId = template.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

        for (String authority : account.getAuthorities()) {
            template.update(SQL_INSERT_AUTHORITIES, newId, authority);
        }
        
        account.setId(newId);
        return account;
    }

    @Override
    public Account read(Integer id) {
        Account account = template.queryForObject(SQL_GET_ACCOUNT, new AccountMapper(), id);
        List<String> authorities = (List<String>) template.queryForList(SQL_GET_AUTHORITIES_LIST, String.class, id);
        account.setAuthorities(authorities);
        return account;

    }

    @Override
    public Account update(Account account) {
        template.update(SQL_UPDATE_ACCOUNT,
                account.getName(),
                account.getEmail(),
                account.getPassword(),
                account.isEnabled(),
                account.getId());
        template.update(SQL_DELETE_AUTHORITIES, account.getId());
        for (String authority : account.getAuthorities()) {
            template.update(SQL_INSERT_AUTHORITIES, account.getId(), authority);
        }
        return account;
    }

    @Override
    public void delete(Integer id) {
        template.update(SQL_DELETE_AUTHORITIES, id);
        template.update(SQL_DELETE_ACCOUNT, id);
    }

    @Override
    public List<Account> list() {
        List<Account> list = template.query(SQL_LIST_ACCOUNTS, new AccountMapper());
        return list;
    }

    @Override
    public Account readByName(String userName) {
        Account account = template.queryForObject(SQL_GET_ACCOUNT_BY_NAME, new AccountMapper(), userName);
        List<String> authorities = (List<String>) template.queryForList(SQL_GET_AUTHORITIES_LIST, String.class, account.getId());
        account.setAuthorities(authorities);
        return account;
    }



    private static class AccountMapper implements RowMapper<Account> {

        public AccountMapper() {
        }

        @Override
        public Account mapRow(ResultSet rs, int i) throws SQLException {
            Account account = new Account();
            account.setEmail(rs.getString("email"));
            account.setName(rs.getString("name"));
            account.setPassword(rs.getString("password"));
            account.setId(rs.getInt("account_id"));
            account.setEnabled(rs.getBoolean("enabled"));
            return account;
        }
    }

}
