/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.dao;

import com.thesoftwareguild.akronautobodycms.dto.Quote;
import com.thesoftwareguild.akronautobodycms.dto.QuoteModalInfo;
import com.thesoftwareguild.akronautobodycms.dto.UploadFile;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class QuoteDao implements QuoteInf {

    private static final String SQL_INSERT_QUOTE = "INSERT INTO quote(year, make, model, bodyStyle, vin, description, userFile, userFile_name, userFile_origName, userFile_contentType, email, phone, name) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private static final String SQL_INSERT_QUOTE_NO_FILE = "INSERT INTO quote(year, make, model, bodyStyle, vin, description, email, phone, name) VALUES(?,?,?,?,?,?,?,?,?)";

    private static final String SQL_READ_QUOTE = "SELECT * FROM quote WHERE quote_id =?";

    private static final String SQL_UPDATE_QUOTE = "UPDATE quote SET year=?,  make=?,  model=?,  bodyStyle=?,  vin=?,  description=?,  userFile=?,  userFile_name=?,  userFile_origName=?,  userFile_contentType=?,  email=?,  phone=?,  name=? WHERE quote_id=?";
    private static final String SQL_UPDATE_QUOTE_NO_FILE = "UPDATE quote SET year=?,  make=?,  model=?,  bodyStyle=?,  vin=?,  description=?,  userFile='',  userFile_name='',  userFile_origName='',  userFile_contentType='',  email=?,  phone=?,  name=? WHERE quote_id=?";
    private static final String SQL_UPDATE_QUOTE_REMOVE_FILE = "UPDATE quote SET userFile=?,  userFile_name=?,  userFile_origName=?,  userFile_contentType=? WHERE quote_id=?";

    private static final String SQL_DELETE_QUOTE = "DELETE FROM quote WHERE quote_id=?";

    private static final String SQL_LIST_QUOTES = "SELECT * FROM quote";

    /*=====Get quote dropdown options=====*/
    //no params (SQL_READ_YEARS)
    private static final String SQL_READ_YEARS = "SELECT DISTINCT year FROM VehicleModelYear;";
    //? = year (SQL_READ_YEAR_MAKES)
    private static final String SQL_READ_YEAR_MAKES = "SELECT DISTINCT make FROM VehicleModelYear WHERE year = ?;";
    //? = year, ? = make (SQL_READ_MAKE_MODELS)
    private static final String SQL_READ_MAKE_MODELS = "SELECT model FROM VehicleModelYear WHERE year = ? AND make = ?;";

    /*Quote File Stuff*/
    private static final String SQL_GET_QUOTE_ATTACHMENT = "SELECT quote_id, userFile, userFile_contentType FROM quote WHERE quote_id = ?;";

    private JdbcTemplate template;
    private MailSender ms;

    public QuoteDao(JdbcTemplate template, MailSender ms) {
        this.template = template;
        this.ms = ms;
    }

    public void sendMsg(Quote quote, String status) {
        String toAddr = "akronautobody.mqkb@gmail.com";
        String fromAddr = "akronautobody.mqkb@gmail.com";
        String subject = status + " Request From \"" + quote.getEmail() + "\"";
        String body = "Car Year: " + quote.getYear()
                + "\nCar Make: " + quote.getMake()
                + "\nCar Model: "+ quote.getModel()
                + "\nBody Style: " + quote.getBodyStyle()
                + "\nVin: " + quote.getVin()
                + "\nDescription: " + quote.getDescription()
                + "\nAttached File: "+(quote.getDownloadLink().length()>1?("http://localhost:8080/AkronAutoBodyCMS/quote/download/" + quote.getId()):"No file to download...")
                + "\nEmail: " + quote.getEmail()
                + "\nPhone: " + quote.getPhone()
                + "\nName: "+ quote.getName();

        SimpleMailMessage crunchifyMsg = new SimpleMailMessage();
        crunchifyMsg.setFrom(fromAddr);
        crunchifyMsg.setTo(toAddr);
        crunchifyMsg.setSubject(subject);
        crunchifyMsg.setText(body);
        ms.send(crunchifyMsg);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Quote create(Quote quote) {
        if (null != quote.getUserFile()) {
            try {
                template.update(SQL_INSERT_QUOTE,
                        quote.getYear(),
                        quote.getMake(),
                        quote.getModel(),
                        quote.getBodyStyle(),
                        quote.getVin(),
                        quote.getDescription(),
                        quote.getUserFile().getBytes(),
                        quote.getUserFile().getName(),
                        quote.getUserFile().getOriginalFilename(),
                        quote.getUserFile().getContentType(),
                        quote.getEmail(),
                        quote.getPhone(),
                        quote.getName()
                );
            } catch (IOException ex) {
                quote.setDownloadLink("");
                Logger.getLogger(QuoteDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            template.update(SQL_INSERT_QUOTE_NO_FILE,
                    quote.getYear(),
                    quote.getMake(),
                    quote.getModel(),
                    quote.getBodyStyle(),
                    quote.getVin(),
                    quote.getDescription(),
                    quote.getEmail(),
                    quote.getPhone(),
                    quote.getName()
            );
        }
        int newId = template.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        quote.setId(newId);

        quote.setDownloadLink("quote/download/" + newId);
        quote.setUserFile(null);

        sendMsg(quote, "New");

        return quote;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Quote update(Quote quote) {
        if ("removed".equals(quote.getDownloadLink()) && null == quote.getUserFile()) {
            template.update(SQL_UPDATE_QUOTE_REMOVE_FILE,
                    null,
                    null,
                    null,
                    null,
                    quote.getId()
            );
        }

        if (null != quote.getUserFile()) {
            try {
                template.update(SQL_UPDATE_QUOTE,
                        quote.getYear(),
                        quote.getMake(),
                        quote.getModel(),
                        quote.getBodyStyle(),
                        quote.getVin(),
                        quote.getDescription(),
                        quote.getUserFile().getBytes(),
                        quote.getUserFile().getName(),
                        quote.getUserFile().getOriginalFilename(),
                        quote.getUserFile().getContentType(),
                        quote.getEmail(),
                        quote.getPhone(),
                        quote.getName(),
                        quote.getId()
                );
            } catch (IOException ex) {
                Logger.getLogger(QuoteDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            template.update(SQL_UPDATE_QUOTE_NO_FILE,
                    quote.getYear(),
                    quote.getMake(),
                    quote.getModel(),
                    quote.getBodyStyle(),
                    quote.getVin(),
                    quote.getDescription(),
                    quote.getEmail(),
                    quote.getPhone(),
                    quote.getName(),
                    quote.getId()
            );
        }

        try {
            if (null != quote.getUserFile().getBytes()) {
                quote.setDownloadLink("quote/download/" + quote.getId());
            }
        } catch (NullPointerException | IOException ex) {
            quote.setDownloadLink("");
        }

        quote.setUserFile(null);

        sendMsg(quote, "Revised");

        return quote;
    }

    @Override
    public UploadFile get(Integer id) {
        UploadFile uploadFile = template.queryForObject(SQL_GET_QUOTE_ATTACHMENT, new UploadFileMapper(), id);

        return uploadFile;
    }

    @Override
    public Quote read(Integer id) {
        Quote quote = template.queryForObject(SQL_READ_QUOTE, new QuoteMapper(), id);

        return quote;
    }

    @Override
    public void delete(Integer id) {
        template.update(SQL_DELETE_QUOTE, id);
    }

    @Override
    public List<Quote> list() {
        List<Quote> list = template.query(SQL_LIST_QUOTES, new QuoteMapper());
        return list;
    }

    @Override
    public QuoteModalInfo getQuoteModalOptions(QuoteModalInfo info) {

        return info;
    }

    @Override
    public QuoteModalInfo getYears() {
        QuoteModalInfo info = new QuoteModalInfo();

        List<Integer> years = (List<Integer>) template.queryForList(SQL_READ_YEARS, Integer.class);

        info.setYears(years);

        return info;
    }

    @Override
    public QuoteModalInfo getMakes(QuoteModalInfo info) {
        List<String> makes = (List<String>) template.queryForList(SQL_READ_YEAR_MAKES, String.class, info.getSelectedYear());

        info.setMakes(makes);

        return info;
    }

    @Override
    public QuoteModalInfo getModels(QuoteModalInfo info) {
        List<String> models = (List<String>) template.queryForList(SQL_READ_MAKE_MODELS, String.class, info.getSelectedYear(), info.getSelectedMake());

        info.setModels(models);

        return info;
    }

    private static class QuoteMapper implements RowMapper<Quote> {

        public QuoteMapper() {
        }

        @Override
        public Quote mapRow(ResultSet rs, int i) throws SQLException {
            Quote q = new Quote();

            q.setId(rs.getInt("quote_id"));
            q.setYear(rs.getInt("year"));
            q.setMake(rs.getString("make"));
            q.setModel(rs.getString("model"));
            q.setBodyStyle(rs.getString("bodyStyle"));
            q.setVin(rs.getString("vin"));
            q.setDescription(rs.getString("description"));
            try {
                if (rs.getString("userFile_contentType").length() > 1) {
                    q.setDownloadLink("quote/download/" + q.getId());
                } else {
                    q.setDownloadLink("");
                }
            } catch (NullPointerException ex) {
                q.setDownloadLink("");
            }
            q.setEmail(rs.getString("email"));
            q.setPhone(rs.getString("phone"));
            q.setName(rs.getString("name"));
            return q;
        }
    }

    private static class UploadFileMapper implements RowMapper<UploadFile> {

        public UploadFileMapper() {
        }

        @Override
        public UploadFile mapRow(ResultSet rs, int i) throws SQLException {
            UploadFile file = new UploadFile();

            file.setId(rs.getInt("quote_id"));
            file.setFileName("quoteFile_" + file.getId());
            file.setFileType(rs.getString("userFile_contentType"));
            file.setData(rs.getBytes("userFile"));

            return file;
        }
    }
}
