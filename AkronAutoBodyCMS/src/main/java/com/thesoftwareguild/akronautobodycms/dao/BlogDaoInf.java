/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.dao;

import com.thesoftwareguild.akronautobodycms.dto.Blog;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface BlogDaoInf extends CRUD<Blog>{
    
    public Blog create (Blog blog);
    
    public Blog read (Integer id);
    
    public Blog update (Blog blog);
    
    public void delete(Integer id); 
    
    public List <Blog> list();
    
    public List <Blog> listAllBlogs();
    
    public List<Blog> listByHashTag(String hashtag);
    
    public List<Blog> listByCategory(String category);
    
    public List<Blog> listByAuthor(String author);
    
    public List<Blog> listByKeyword(String keyword);
}
