/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.dao;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import com.thesoftwareguild.akronautobodycms.dto.Category;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class CategoryDao implements CategoryInf {

    private static final String SQL_INSERT_CATEGORY = "INSERT INTO `category` (`name`) VALUES (?)";
    private static final String SQL_GET_CATEGORY = "SELECT* FROM  category WHERE category_id =?";
    private static final String SQL_UPDATE_CATEGORY = "UPDATE category SET name=? WHERE category_id =?";
    private static final String SQL_DELETE_CATEGORY = "DELETE FROM category WHERE category_id=?";
    private static final String SQL_LIST_CATEGORY = "SELECT*FROM category";

    private static final String SQL_COUNT_DEPENDENCIES = "SELECT COUNT(*) FROM blog WHERE category_id = ?;";

    private JdbcTemplate template;

    public CategoryDao(JdbcTemplate template) {
        this.template = template;
    }

    @Override
    public boolean isFree(Integer id) {
        return (!(template.queryForObject(SQL_COUNT_DEPENDENCIES, Integer.class, id) >= 1));
    }

    @Override
    public Category create(Category category) {
        template.update(SQL_INSERT_CATEGORY,
                category.getCategory());

        category.setId(template.queryForObject("SELECT LAST_INSERT_ID()", Integer.class));
        return category;
    }

    @Override
    public Category read(Integer id) {
        return template.queryForObject(SQL_GET_CATEGORY, new CategoryMapper(), id);
    }

    @Override
    public Category update(Category category) {
        template.update(SQL_UPDATE_CATEGORY,
                category.getCategory(),
                category.getId());

        return category;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(Integer id) {
        template.update(SQL_DELETE_CATEGORY, id);
    }

    @Override
    public List<Category> list() {

        return template.query(SQL_LIST_CATEGORY, new CategoryMapper());

    }

    private static class CategoryMapper implements RowMapper<Category> {

        public CategoryMapper() {
        }

        @Override
        public Category mapRow(ResultSet rs, int i) throws SQLException {

            Category category = new Category();
            category.setId(rs.getInt("category_id"));
            category.setCategory(rs.getString("name"));

            return category;

        }
    }
}
