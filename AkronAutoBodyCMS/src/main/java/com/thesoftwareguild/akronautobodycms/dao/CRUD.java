/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.dao;

import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;



/**
 *
 * @author apprentice
 */
public interface CRUD<T> {
    
    public T create (T o);
    
    public T read (Integer id);
    
    public T update (T o);
    
    public void delete(Integer id); 
    
    public List <T> list();
    
    
}
