/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.dao;

import com.thesoftwareguild.akronautobodycms.dto.Blog;
import com.thesoftwareguild.akronautobodycms.dto.Category;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface CategoryInf extends CRUD<Category>{
    
    public boolean isFree(Integer id);
    
    public Category create (Category category);
    
    public Category read (Integer id);
    
    public Category update (Category category);
    
    public void delete(Integer id); 
    
    public List <Category> list();
    
}
