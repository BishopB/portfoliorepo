/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.dao;

import com.thesoftwareguild.akronautobodycms.dto.Account;
import com.thesoftwareguild.akronautobodycms.dto.StaticPage;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author apprentice
 */
public class StaticPageDao implements StaticPageInf {

    private static final String SQL_INSERT_STATIC_PAGE = "INSERT INTO staticPage (title, content, author_id)VALUES(?,?,?)";
    private static final String SQL_GET_STATIC_PAGE = "SELECT * FROM staticPage sp INNER JOIN account act ON act.account_id = sp.author_id WHERE sp.staticPage_id = ?;";
    private static final String SQL_UPDATE_STATIC_PAGE = "UPDATE staticPage SET title=?, content=?, author_id=? WHERE staticPage_id =?";
    private static final String SQL_DELETE_STATIC_PAGE = "DELETE FROM staticPage WHERE staticPage_id =?";
    private static final String SQL_LIST_STATIC_PAGES = "SELECT * FROM staticPage sp INNER JOIN account act ON act.account_id=sp.author_id";

    private JdbcTemplate template;

    public StaticPageDao(JdbcTemplate template) {
        this.template = template;
    }

    @Override
    public StaticPage create(StaticPage staticPage) {

        try {
            template.update(SQL_INSERT_STATIC_PAGE,
                    staticPage.getTitle(),
                    staticPage.getContent(),
                    staticPage.getAuthor().getId());

            staticPage.setId(template.queryForObject("SELECT LAST_INSERT_ID()", Integer.class));

            return staticPage;
        } catch (Exception e) {
            return null;
        }

    }

    @Override
    public StaticPage read(Integer id) {
        return template.queryForObject(
                SQL_GET_STATIC_PAGE,
                new StaticPageMapper(),
                id);

    }

    @Override
    public StaticPage update(StaticPage staticPage) {
        try {
            template.update(SQL_UPDATE_STATIC_PAGE,
                    staticPage.getTitle(),
                    staticPage.getContent(),
                    staticPage.getAuthor().getId(),
                    staticPage.getId());

            return staticPage;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void delete(Integer id) {
        template.update(SQL_DELETE_STATIC_PAGE, id);
    }

    @Override
    public List<StaticPage> list() {
        List<StaticPage> list = template.query(SQL_LIST_STATIC_PAGES, new StaticPageMapper());
        return list;
    }

    private class StaticPageMapper implements RowMapper<StaticPage> {

//        public StaticPageMapper() {
//        }
        @Override
        public StaticPage mapRow(ResultSet rs, int i) throws SQLException {

            Account pageAuthor = new Account();
            pageAuthor.setEmail(rs.getString("email"));
            pageAuthor.setId(rs.getInt("account_id"));
            pageAuthor.setPassword("");
            pageAuthor.setName(rs.getString("name"));

            StaticPage staticPage = new StaticPage();
            staticPage.setId(rs.getInt("staticPage_id"));
            staticPage.setContent(rs.getString("content"));
            staticPage.setTitle(rs.getString("title"));

            staticPage.setAuthor(pageAuthor);

            return staticPage;
        }
    }

}
