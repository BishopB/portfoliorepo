/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.controller;

import com.thesoftwareguild.akronautobodycms.dao.FileUploadDao;
import com.thesoftwareguild.akronautobodycms.dto.UploadFile;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/uploadFile")
public class FileUploadController {

    private FileUploadDao uploadDao;

    @Inject
    public FileUploadController(FileUploadDao daoBean) {
        this.uploadDao = daoBean;
    }

    @RequestMapping(value = "/showImage/{id}", method = RequestMethod.GET)
    public void showImage(@PathVariable("id") Integer id, HttpServletResponse response) throws IOException {

        UploadFile file = uploadDao.get(id);
        IOUtils.copy(new ByteArrayInputStream(file.getData()), response.getOutputStream());
        response.setContentType(file.getFileType());
    }

    @RequestMapping(value = "/download/{id}", method = RequestMethod.GET)
    public void downloadFile(@PathVariable("id") Integer id, HttpServletResponse response) throws IOException {
        byte[] file = uploadDao.get(id).getData();
        response.reset();
        response.setBufferSize(4096);
        response.setContentType(uploadDao.get(id).getFileType());
        /*Removing the below line will render in browser, adding it causes download instead*/
        response.setHeader("Content-Disposition", "attachment; filename="+uploadDao.get(id).getFileName()); 
        try {
            response.getOutputStream().write(file);
        } catch (IOException e) {
            // Do something
        }
    }
    @RequestMapping(value = "/forPlugin", method = RequestMethod.POST)
    public String uploadFileHandlerForPlugin(Model model, @RequestParam("userfile") MultipartFile file) {
        UploadFile uploadFile = new UploadFile();

        if (!file.isEmpty()) {
            try {
                uploadFile.setData(file.getBytes());
                uploadFile.setFileName(file.getName());
                uploadFile.setFileType(file.getContentType());
                uploadFile = uploadDao.add(uploadFile);

                model.addAttribute("file_name", "uploadFile/showImage/" + uploadFile.getId());
                model.addAttribute("result", "success");
                model.addAttribute("resultcode", "success");

                return "uploadResult";
            } catch (Exception e) {
            }
        }

        model.addAttribute("");
        model.addAttribute("result", "failed");
        model.addAttribute("resultcode", "failed");

        return "uploadResult";
    }
}
