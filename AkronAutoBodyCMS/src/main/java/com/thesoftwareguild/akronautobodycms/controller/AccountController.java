/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.controller;

import com.thesoftwareguild.akronautobodycms.dao.AccountInf;
import com.thesoftwareguild.akronautobodycms.dto.Account;
import com.thesoftwareguild.akronautobodycms.validation.AccountUniqueValidation;
import com.thesoftwareguild.akronautobodycms.viewModel.AccountCommandObject;
import com.thesoftwareguild.akronautobodycms.viewModel.AccountEditObject;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/account")
public class AccountController implements AdminControllerInterface<Account> {

    @Autowired
    AccountUniqueValidation accountValidator;
    private AccountInf dao;
    private PasswordEncoder encoder;

    @Inject
    public AccountController(AccountInf dao, PasswordEncoder encoder) {
        this.dao = dao;
        this.encoder = encoder;
    }

//    @InitBinder
//    public void initBinder(WebDataBinder webDataBinder){
//        webDataBinder.setValidator(accountValidator);
//    }
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String home(Model model) {
        List<Account> listAccounts = dao.list();
        model.addAttribute("accountsList", listAccounts);

        return "Account";
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public AccountCommandObject create(@Valid @RequestBody AccountCommandObject o) {
        Account account = new Account();
        account.setName(o.getName());
        account.setEmail(o.getEmail());

        account.setPassword(encoder.encode(o.getPassword()));

        if (o.getEnabled().equals("enabled")) {
            account.setEnabled(true);
        } else {
            o.setEnabled("disabled");
            account.setEnabled(false);
        }

        if (o.getAdmin().equals("ROLE_ADMIN")) {
            account.getAuthorities().add("ROLE_ADMIN");
            account.getAuthorities().add("ROLE_SUB_ADMIN");
        } else if (o.getAdmin().equals("ROLE_SUB_ADMIN")) {
            account.getAuthorities().add("ROLE_SUB_ADMIN");
        }

        Account addedAccount = dao.create(account);
        o.setId(addedAccount.getId());

        return o;

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public AccountCommandObject read(@PathVariable("id") Integer x_id) {
        Account account = dao.read(x_id);
        AccountCommandObject ACO = new AccountCommandObject();
        ACO.setId(account.getId());
        ACO.setName(account.getName());
        ACO.setEmail(account.getEmail());
        if (account.isEnabled()) {
            ACO.setEnabled("enabled");
        } else {
            ACO.setEnabled("disabled");
        }

        for (String authority : account.getAuthorities()) {
            if (authority.equals("ROLE_ADMIN")) {
                ACO.setAdmin("ROLE_ADMIN");
            } else if (authority.equals("ROLE_SUB_ADMIN")) {
                ACO.setAdmin("ROLE_SUB_ADMIN");
            }
        }

        return ACO;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public AccountEditObject update(@Valid @RequestBody AccountEditObject o, @PathVariable("id") Integer id) {
        if (dao.editUnique(o)) {

            Account account = new Account();
            account.setId(id);
            account.setPassword(encoder.encode(o.getPassword()));
            account.setName(o.getName());
            account.setEmail(o.getEmail());
            if (o.getEnabled().equals("enabled")) {
                account.setEnabled(true);
            } else {
                o.setEnabled("disabled");
                account.setEnabled(false);
            }

            if (o.getAdmin().equals("ROLE_ADMIN")) {
                account.getAuthorities().add("ROLE_ADMIN");
                account.getAuthorities().add("ROLE_SUB_ADMIN");
            } else if (o.getAdmin().equals("ROLE_SUB_ADMIN")) {
                account.getAuthorities().add("ROLE_SUB_ADMIN");
            }

            dao.update(account);
            o.setPassword("success");
            return o;
        }
        AccountEditObject aeo = new AccountEditObject();
        aeo.setPassword("fail");
        return aeo;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable("id") Integer x_id) {
        dao.delete(x_id);
    }

    public List<Account> search(String s, String type) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @RequestMapping(value = "/isFree/{id}", method = RequestMethod.GET)
    @ResponseBody
    public boolean isFree(@PathVariable("id") Integer id) {
        return dao.isFree(id);
    }

    public boolean isEmailUnique(String email) {
        return true;
    }

    public boolean isLoginValid(String login) {
        return true;
    }
}
