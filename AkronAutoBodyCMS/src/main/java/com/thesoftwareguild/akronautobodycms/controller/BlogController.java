/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.controller;

import com.thesoftwareguild.akronautobodycms.dao.AccountInf;
import com.thesoftwareguild.akronautobodycms.dao.BlogDaoInf;
import com.thesoftwareguild.akronautobodycms.dao.CategoryInf;
import com.thesoftwareguild.akronautobodycms.dto.Blog;
import com.thesoftwareguild.akronautobodycms.viewModel.BlogCommand;
import com.thesoftwareguild.akronautobodycms.viewModel.BlogViewModel;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/blog")
public class BlogController implements AdminControllerInterface<Blog> {

    Integer id;
    String title;
    String content;

    BlogDaoInf dao;
    AccountInf accountDao;
    CategoryInf categoryDao;

    @Inject
    public BlogController(BlogDaoInf dao, AccountInf accountDao, CategoryInf categoryDao) {
        this.dao = dao;
        this.accountDao = accountDao;
        this.categoryDao = categoryDao;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String home(Model model) {
        List<Blog> listBlogs = dao.listAllBlogs();
        model.addAttribute("blogsList", listBlogs);
        model.addAttribute("categoryList", categoryDao.list());

        return "BlogAdmin";
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public Blog create(@Valid @RequestBody BlogCommand command) {
        Blog o = new Blog();
        o.setAccount(accountDao.read(command.getAccountId()));
        o.setCategory(categoryDao.read(command.getCategoryId()));
        o.setAnchor("nope");
        o.setContent(command.getContent());
        o.setTitle(command.getTitle());
        o.setEndDate(command.getEndDate());
        o.setStartDate(command.getStartDate());
        List<String> hashtaglist = new ArrayList();
        hashtaglist.add("nope");
        o.setHashtag(hashtaglist);
        o.setApproved(command.getApproved());
        Blog addedBlog = dao.create(o);
        return addedBlog;
    }

    @RequestMapping(value = "/read/{id}", method = RequestMethod.GET)
    @ResponseBody
    public BlogViewModel read(@PathVariable("id") Integer x_id) {

        BlogViewModel model = new BlogViewModel();
        Blog blog = dao.read(x_id);
        model.setTitle(blog.getTitle());
        model.setStartDate(blog.getStartDate().toString());
        model.setEndDate(blog.getEndDate().toString());
        model.setContent(blog.getContent());
        model.setApproval(blog.getApproved());
        model.setCategoryId(blog.getCategory().getId().toString());

        return model;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public Blog update(@Valid @RequestBody BlogCommand command, @PathVariable("id") Integer id) {
        Blog blog = new Blog();
        blog.setId(id);
        blog.setEndDate(command.getEndDate());
        blog.setStartDate(command.getStartDate());
        blog.setContent(command.getContent());
        blog.setTitle(command.getTitle());
        blog.setAccount(accountDao.read(command.getAccountId()));
        blog.setCategory(categoryDao.read(command.getCategoryId()));
        blog.setAnchor("nope");
        List<String> hashtaglist = new ArrayList();
        hashtaglist.add("nope");
        blog.setHashtag(hashtaglist);
        blog.setApproved(command.getApproved());
        return dao.update(blog);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable("id") Integer x_id) {
        dao.delete(x_id);
    }

    public List<Blog> search(String s, String type) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Blog> listBlog() {
        return null;
    }

    public List<Blog> listByHashTag(String hashtag) {
        return null;
    }

    public void expire() {

    }

    public List<Blog> listByCategory(String category) {
        return null;
    }

    public List<Blog> listByAuthor(String author) {
        return null;
    }

    public List<Blog> listByKeyword(String keyword) {
        return null;
    }
}
