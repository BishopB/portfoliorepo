/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.controller;

import com.thesoftwareguild.akronautobodycms.dao.AccountInf;
import com.thesoftwareguild.akronautobodycms.dto.Account;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
public class LoginController {

    AccountInf accountDao;

    @Inject
    public LoginController(AccountInf accountDao) {
        this.accountDao = accountDao;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {

        return "login";
    }

    @RequestMapping(value = "/username", method = RequestMethod.GET)
    @ResponseBody
    public List<GrantedAuthority> currentUserName(Authentication authentication) {
        User account = (User) authentication.getPrincipal();
        authentication.getAuthorities();

        return getAuthoritiesList(authentication);
    }

    @RequestMapping(value = "/userId", method = RequestMethod.GET)
    @ResponseBody
    public Account currentUserAccount(Authentication authentication) {
        try {
            String userName = authentication.getName();
            return accountDao.readByName(userName);
        } catch (NullPointerException ex) {
            return new Account();
        }
    }

    public List<GrantedAuthority> getAuthoritiesList(Authentication authentication) {
        //    GrantedAuthority alist =  authentication.getAuthorities();
        List<GrantedAuthority> alist = new ArrayList<>();
        for (GrantedAuthority auth : authentication.getAuthorities()) {
            alist.add(auth);
        }
        return alist;
    }

}
