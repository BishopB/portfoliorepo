/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.controller;

import com.thesoftwareguild.akronautobodycms.dao.CategoryInf;
import com.thesoftwareguild.akronautobodycms.dto.Category;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/category")
public class CategoryController implements AdminControllerInterface<Category> {

    CategoryInf dao;

    @Inject
    public CategoryController(CategoryInf dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/isFree/{id}", method = RequestMethod.GET)
    @ResponseBody
    public boolean isFree(@PathVariable("id") Integer id) {
        return dao.isFree(id);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String home(Model model) {
        List<Category> listCategory = dao.list();
        model.addAttribute("categorysList", listCategory);

        return "CategoryAdmin";
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public Category create(@Valid @RequestBody Category o) {
        return dao.create(o);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Category read(@PathVariable("id") Integer x_id) {
        return dao.read(x_id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public Category update(@RequestBody Category o, @PathVariable("id") Integer id) {
        o.setId(id);
        return dao.update(o);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable("id") Integer x_id) {
        dao.delete(x_id);
    }

    public List<Category> search(String s, String type) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
