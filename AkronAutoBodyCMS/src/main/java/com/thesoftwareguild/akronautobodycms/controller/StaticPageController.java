/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.controller;

import com.thesoftwareguild.akronautobodycms.dao.AccountInf;
import com.thesoftwareguild.akronautobodycms.dao.StaticPageInf;
import com.thesoftwareguild.akronautobodycms.dto.StaticPage;
import com.thesoftwareguild.akronautobodycms.viewModel.StaticPageCommand;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value="/staticPage")
public class StaticPageController implements AdminControllerInterface<StaticPage> {

    StaticPageInf dao;
    AccountInf accountDao;
    
    @Inject
    public StaticPageController(StaticPageInf dao, AccountInf accountDao){
        this.dao = dao;
        this.accountDao = accountDao;
    }
    

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String home(Model model) {
      List<StaticPage> listStaticPage = dao.list();
      model.addAttribute("StaticPageList", listStaticPage);
        return "StaticPageAdmin";
    }
    
    
    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public StaticPage create(@Valid @RequestBody StaticPageCommand command) {
        StaticPage o = new StaticPage();
        o.setId(command.getId());
        o.setAuthor(accountDao.read(command.getAccountId()));
        o.setTitle(command.getTitle());
        o.setContent(command.getContent());
        StaticPage added = dao.create(o);
        
        return added;
    }

    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    @ResponseBody
    public StaticPage read(@PathVariable("id") Integer x_id) {
        return dao.read(x_id);
    }

    @RequestMapping(value="/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public StaticPage update(@Valid @RequestBody StaticPage o, @PathVariable("id") Integer id) {
        o.setId(id);
        o.setAuthor(accountDao.read(1));
        return dao.update(o);
    }

    @RequestMapping(value="/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable("id") Integer x_id) {
        dao.delete(x_id);
    }

    
    public List<StaticPage> search(String s, String type) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    @RequestMapping(value = "/", method=RequestMethod.GET)
    public String listStaticPages(Model model){
        List<StaticPage> listStaticPages = dao.list();
        model.addAttribute("AdminStaticPagesTable", listStaticPages);
        
        return "StaticPageAdmin";
    }
 
    
}
