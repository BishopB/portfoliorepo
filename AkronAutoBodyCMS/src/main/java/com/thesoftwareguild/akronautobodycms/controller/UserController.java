
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.controller;

import com.thesoftwareguild.akronautobodycms.dao.AccountInf;
import com.thesoftwareguild.akronautobodycms.dao.BlogDaoInf;
import com.thesoftwareguild.akronautobodycms.dao.CategoryInf;
import com.thesoftwareguild.akronautobodycms.dao.QuoteInf;
import com.thesoftwareguild.akronautobodycms.dao.StaticPageInf;
import com.thesoftwareguild.akronautobodycms.dto.Blog;
import com.thesoftwareguild.akronautobodycms.dto.StaticPage;
import java.util.Collections;
import java.util.List;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
public class UserController {

    AccountInf accountInf;
    BlogDaoInf blogInf;
    CategoryInf categoryInf;
    QuoteInf quoteInf;
    StaticPageInf staticPageInf;

    @Inject

    public UserController(AccountInf accountInf, BlogDaoInf blogInf, CategoryInf categoryInf, QuoteInf quoteInf, StaticPageInf staticPageInf) {
        this.accountInf = accountInf;
        this.blogInf = blogInf;
        this.categoryInf = categoryInf;
        this.quoteInf = quoteInf;
        this.staticPageInf = staticPageInf;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public StaticPage getStaticPage(@PathVariable("id") Integer id) {
        return staticPageInf.read(id);
    }

    public List<Blog> search(String search) {
        return null;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String listBlogPosts(Model model) {
        List<Blog> listBlogs = blogInf.list();
        model.addAttribute("UserBlogsTable", listBlogs);
        
        List<StaticPage> listStaticPage = staticPageInf.list();
        model.addAttribute("StaticPageList", listStaticPage);
        return "UserView";
    }

    @RequestMapping(value = "/dropdown", method = RequestMethod.GET)
    @ResponseBody
    public List<StaticPage> dropDownGenerator() {
        List<StaticPage> copyList = staticPageInf.list();

        
        Collections.sort(copyList, (StaticPage o1, StaticPage o2) -> o1.getId().compareTo(o2.getId()));
        return copyList;
    }

}
