/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.controller;

import com.thesoftwareguild.akronautobodycms.dao.BlogDaoInf;
import com.thesoftwareguild.akronautobodycms.dto.Blog;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/search")
public class SearchController {

    private BlogDaoInf dao;

    @Inject
    public SearchController(BlogDaoInf blogDao) {
        this.dao = blogDao;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public String searchByHashTag(@RequestParam(value = "searchType") String searchType, @RequestParam(value = "searchValue") String searchValue, Model model) {
        List<Blog> results = new ArrayList();

        switch (searchType) {
            case "Hashtag":
                results = dao.listByHashTag(searchValue);
                break;

            case "Category":
                results = dao.listByCategory(searchValue);
                break;

            case "Author":
                results = dao.listByAuthor(searchValue);
                break;

            case "Keyword":
                results = dao.listByKeyword(searchValue);
                break;
        }

        model.addAttribute("results", results);
        model.addAttribute("searchType", searchType);
        model.addAttribute("searchValue", searchValue);

        return "search_1";
    }

}
