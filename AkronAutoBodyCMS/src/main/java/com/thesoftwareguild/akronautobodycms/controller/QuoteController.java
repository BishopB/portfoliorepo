/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.controller;

import com.thesoftwareguild.akronautobodycms.dao.QuoteInf;
import com.thesoftwareguild.akronautobodycms.dto.Quote;
import com.thesoftwareguild.akronautobodycms.dto.QuoteModalInfo;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value = "/quote")
public class QuoteController implements AdminControllerInterface<Quote> {

    private QuoteInf dao;
    private MailSender ms;

    @Inject
    public QuoteController(QuoteInf dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String home(Model model) {
        List<Quote> quoteList = dao.list();
        model.addAttribute("quoteList", quoteList);

        return "Quote";
    }

    @RequestMapping(value = "/modalInfo", method = RequestMethod.POST)
    @ResponseBody
    public QuoteModalInfo modalInfo(@RequestBody QuoteModalInfo info) {
        if (null != info.getSelectedMake()) {
            info = dao.getModels(info);
        } else {
            info = dao.getMakes(info);
        }
        return info;
    }

    @RequestMapping(value = "/modalInfo", method = RequestMethod.GET)
    @ResponseBody
    public QuoteModalInfo modalInfo() {
        QuoteModalInfo newInfo = dao.getYears();

        return newInfo;
    }

    @RequestMapping(value = "/modalUploadObject", method = RequestMethod.POST)
    @ResponseBody
    public Quote create(@ModelAttribute Quote quote) {

        quote = dao.create(quote);

        return quote;
    }

    @RequestMapping(value = "/modalUploadObject/update", method = RequestMethod.POST)
    @ResponseBody
    public Quote update(@ModelAttribute Quote quote) {

        quote = dao.update(quote);

        return quote;
    }

    @RequestMapping(value = "/download/{id}", method = RequestMethod.GET)
    public void downloadQuoteFile(@PathVariable("id") Integer id, HttpServletResponse response) throws IOException {
        byte[] file = dao.get(id).getData();
        response.reset();
        response.setBufferSize(4096);
        response.setContentType(dao.get(id).getFileType());
        /*Removing the below line will render in browser, adding it causes download instead*/
        response.setHeader("Content-Disposition", "attachment; filename=" + dao.get(id).getFileName());
        try {
            response.getOutputStream().write(file);
        } catch (IOException e) {
            // Do something
        }
    }

    @RequestMapping(value = "/show/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Quote read(@PathVariable("id") Integer x_id) {
        Quote quote = dao.read(x_id);

        return quote;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable("id") Integer x_id) {
        dao.delete(x_id);
    }

}
