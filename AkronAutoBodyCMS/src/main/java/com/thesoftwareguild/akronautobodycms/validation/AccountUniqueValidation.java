///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.thesoftwareguild.akronautobodycms.validation;
//
//import com.thesoftwareguild.akronautobodycms.dao.AccountInf;
//import com.thesoftwareguild.akronautobodycms.dto.Account;
//import javax.inject.Inject;
//import org.springframework.stereotype.Component;
//import org.springframework.validation.Errors;
//import org.springframework.validation.ValidationUtils;
//import org.springframework.validation.Validator;
//
///**
// *
// * @author apprentice
// */
//@Component
//public class AccountUniqueValidation implements Validator {
//
//    AccountInf dao;
//
//    @Inject
//    public AccountUniqueValidation(AccountInf dao) {
//        this.dao = dao;
//    }
//
//    @Override
//    public boolean supports(Class<?> type) {
//        return Account.class.isAssignableFrom(type);
//    }
//
//    @Override
//    public void validate(Object o, Errors errors) {
//        Account account = (Account) o;
//
//        String email = account.getEmail();
//        String password = account.getPassword();
//        String name = account.getName();
//        Boolean enabled = account.isEnabled();
//        Boolean admin = account.isAdmin();
//
//        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "Please Enter An Email");
//        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "**Password Is Required");
//        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "**Name Is Required");
//        
//        if
//    }
//
//}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.validation;

import com.thesoftwareguild.akronautobodycms.dao.AccountInf;
import javax.inject.Inject;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.springframework.stereotype.Component;

/**
 *
 * @author apprentice
 */
@Component
public class AccountUniqueValidation implements ConstraintValidator<UniqueUserName, String> {

    AccountInf dao;
    String email;
    @Inject
    public AccountUniqueValidation(AccountInf dao) {
        this.dao = dao;
    }

    @Override
    public boolean isValid(String t, ConstraintValidatorContext cvc) {
        return !dao.list()
                .stream()
                .map(acc-> acc.getEmail())
                .anyMatch(userName -> userName.equals(t));
    }

    @Override
    public void initialize(UniqueUserName a) {
        this.email = a.toString();
    }

}
