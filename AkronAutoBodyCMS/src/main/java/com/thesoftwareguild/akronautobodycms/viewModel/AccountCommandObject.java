/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.viewModel;

import com.thesoftwareguild.akronautobodycms.validation.UniqueUserName;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author apprentice
 */

public class AccountCommandObject {

    private Integer id;

//    @NotEmpty(message = "Please Enter A Email!")
//    @Column(name = "email", unique = true)
    @UniqueUserName()
    private String email;
    @NotEmpty(message = "Please Enter A Password!")
    private String password;
    @NotEmpty(message = "Please Enter A Name!")
    private String name;
    private String enabled = "";
    private String admin;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

}
