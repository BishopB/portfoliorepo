/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.dto;

import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author apprentice
 */
public class Quote {

    private Integer id;
    private Integer year;
    private String make;
    private String model;
    private String bodyStyle;
    private String vin;
    private String description;
    private MultipartFile userFile = null;
    private String downloadLink;
    private String email;
    private String phone;
    private String name;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the year
     */
    public Integer getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(Integer year) {
        this.year = year;
    }

    /**
     * @return the make
     */
    public String getMake() {
        return make;
    }

    /**
     * @param make the make to set
     */
    public void setMake(String make) {
        this.make = make;
    }

    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * @return the bodyStyle
     */
    public String getBodyStyle() {
        return bodyStyle;
    }

    /**
     * @param bodyStyle the bodyStyle to set
     */
    public void setBodyStyle(String bodyStyle) {
        this.bodyStyle = bodyStyle;
    }

    /**
     * @return the vin
     */
    public String getVin() {
        return vin;
    }

    /**
     * @param vin the vin to set
     */
    public void setVin(String vin) {
        this.vin = vin;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the userFile
     */
    public MultipartFile getUserFile() {
        return userFile;
    }

    /**
     * @param userFile the userFile to set
     */
    public void setUserFile(MultipartFile userFile) {
        this.userFile = userFile;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the downloadLink
     */
    public String getDownloadLink() {
        return downloadLink;
    }

    /**
     * @param downloadLink the downloadLink to set
     */
    public void setDownloadLink(String downloadLink) {
        this.downloadLink = downloadLink;
    }

}
