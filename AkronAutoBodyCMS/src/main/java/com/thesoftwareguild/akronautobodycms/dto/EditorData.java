/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.dto;

/**
 *
 * @author apprentice
 */
public class EditorData {
    //This classed is used to store the String content passed from the Editor to the controllers
    //Date, Title, category attributes can also be stored in this container
    
    private String contentFromTinyMce;

    /**
     * @return the contentFromTinyMce
     */
    public String getContentFromTinyMce() {
        return contentFromTinyMce;
    }

    /**
     * @param contentFromTinyMce the contentFromTinyMce to set
     */
    public void setContentFromTinyMce(String contentFromTinyMce) {
        this.contentFromTinyMce = contentFromTinyMce;
    }
    
}
