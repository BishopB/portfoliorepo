/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.akronautobodycms.dto;

import java.util.List;

/**
 *
 * @author apprentice
 */
public class QuoteModalInfo {
    
    private List<Integer> years;
    private List<String> makes;
    private List<String> models;

    private Integer selectedYear;
    private String selectedMake;
    private String selectedModel;
    
    /**
     * @return the years
     */
    public List<Integer> getYears() {
        return years;
    }

    /**
     * @param years the years to set
     */
    public void setYears(List<Integer> years) {
        this.years = years;
    }

    /**
     * @return the makes
     */
    public List<String> getMakes() {
        return makes;
    }

    /**
     * @param makes the makes to set
     */
    public void setMakes(List<String> makes) {
        this.makes = makes;
    }

    /**
     * @return the models
     */
    public List<String> getModels() {
        return models;
    }

    /**
     * @param models the models to set
     */
    public void setModels(List<String> models) {
        this.models = models;
    }

    /**
     * @return the selectedYear
     */
    public Integer getSelectedYear() {
        return selectedYear;
    }

    /**
     * @param selectedYear the selectedYear to set
     */
    public void setSelectedYear(Integer selectedYear) {
        this.selectedYear = selectedYear;
    }

    /**
     * @return the selectedMake
     */
    public String getSelectedMake() {
        return selectedMake;
    }

    /**
     * @param selectedMake the selectedMake to set
     */
    public void setSelectedMake(String selectedMake) {
        this.selectedMake = selectedMake;
    }

    /**
     * @return the selectedModel
     */
    public String getSelectedModel() {
        return selectedModel;
    }

    /**
     * @param selectedModel the selectedModel to set
     */
    public void setSelectedModel(String selectedModel) {
        this.selectedModel = selectedModel;
    }
    
}
