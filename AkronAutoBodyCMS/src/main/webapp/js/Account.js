$(document).ready(function () {
    $(document).on('submit', "#create-form", function (e) {
        e.preventDefault();

        console.log("admin: " + $('.admin:checked').val() + " - enabled: " + ('enabled' === $('#enabled:checked').val() ? 'enabled' : 'disabled'));


        $('#add-account-validation-errors').empty();

        var accountData = JSON.stringify({
            name: $('#add-name').val(),
            email: $('#add-email').val(),
            password: $('#create-password').val(),
            enabled: ('enabled' === $('#enabled:checked').val() ? 'enabled' : 'disabled'),
            admin: $('.admin:checked').val()

        });

        $.ajax({
            type: 'POST',
            url: contextRoot + "/account",
            data: accountData,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            }
        }).success(function (data, status) {
            $('#add-name').val("");
            $('#add-email').val("");
            $('#create-password').val("");
            $('#enabled').attr('checked', false);
            $('#create-account-admin').attr('checked', true);
            $('#addAccountModal').modal('hide');

            var tableRow = buildAccountRow(data);
            $('#AccountTable').append($(tableRow));
        }).error(function (data, status) {

            var errors = data.responseJSON.errors;

            $.each(errors, function (index, validationError) {
                $('#add-account-validation-errors').append(validationError.fieldName + ": " + validationError.message).append("<br/>");
            });
        });
    });

    $('#showAccountModal').on('show.bs.modal', function (e) {

        var link = $(e.relatedTarget);

        var accountId = link.data('account-id');

        var modal = $(this);

        $.ajax({
            type: 'GET',
            url: contextRoot + "/account/" + accountId,
            datatype: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (account, status) {
            modal.find('#account-name').text(account.name);
            modal.find('#account-email').text(account.email);
            modal.find('#account-authorization').text(account.admin);
            modal.find('#account-enabled').text(account.enabled);
        });
    });

    $('#editAccountModal').on('show.bs.modal', function (e) {
        $('#edit-account-validation-errors').empty();
        var link = $(e.relatedTarget);

        var accountId = link.data('account-id');

        var modal = $(this);

        $.ajax({
            type: 'GET',
            url: contextRoot + "/account/" + accountId,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (account, status) {

            modal.find('#edit-name').val(account.name);
            modal.find('#edit-email').val(account.email);
            modal.find('#edit-password').val("");
            if (account.admin === "ROLE_ADMIN") {
                modal.find('.edit-admin').filter('[value=ROLE_ADMIN]').prop('checked', true);
            }
            if (account.admin === "ROLE_SUB_ADMIN") {
                modal.find('.edit-admin').filter('[value=ROLE_SUB_ADMIN]').prop('checked', true);
            }
            if (account.enabled === "enabled") {
//            modal.find('.edit-enabled').val(account.enabled);
                modal.find('.edit-enabled').filter('[value=enabled]').attr('checked', true);
            }
            if (account.enabled === "disabled") {
                modal.find('.edit-enabled').filter('[value=disabled]').attr('checked', true);
            }
            $('#edit-id').val(account.id);

        });
    });



    $(document).on('click', '#edit-account-button', function (e) {

        e.preventDefault();

        var accountId = $('#edit-id').val();

        $.ajax({
            type: 'PUT',
            url: contextRoot + "/account/" + accountId,
            data: JSON.stringify({
                id: accountId,
                name: $('#edit-name').val(),
                email: $('#edit-email').val(),
                password: $('#edit-password').val(),
                enabled: ('enabled' === $('.edit-enabled:checked').val() ? 'enabled' : 'disabled'),
                admin: $('.edit-admin:checked').val()
            }),
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            }
        }).success(function (data, status) {
            if (data.password === 'fail') {
                $('#edit-account-validation-errors').append("NOT A UNIQUE USERNAME!");
            } else {
                $('#editAccountModal').modal('hide');

                var tableRow = buildAccountRow(data);

                $('#accounts-row-' + data.id).replaceWith($(tableRow));
            }


        }).error(function (data, status) {

            var errors = data.responseJSON.errors;

            $.each(errors, function (index, validationError) {
                $('#edit-account-validation-errors').append(validationError.fieldName + ": " + validationError.message).append("<br/>");
            });
        });
    });

    $(document).on('click', '.delete-link', function (e) {
        e.preventDefault();
        var accountId = $(e.target).data('account-id');

        $.ajax({
            type: 'GET',
            url: contextRoot + "/account/isFree/" + accountId,
            datatype: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (data, status) {
            if (data) {
                $.ajax({
                    type: 'DELETE',
                    url: contextRoot + "/account/" + accountId,
                }).success(function (data, status) {
                    $('#accounts-row-' + accountId).remove();
                });
            } else {
                alert('An account cannot be removed if it is the author of a blog...');
            }
        });
    });




    function buildAccountRow(data) {
        return    "<tr id='accounts-row-" + data.id + "'> \n\
            <td><a data-account-id ='" + data.id + "' class = 'viewAccountLink' data-toggle='modal' data-target='#showAccountModal'>" + data.name + "</a></td> \n\
            <td><a data-account-id ='" + data.id + "' class = 'editAccountLink' data-toggle='modal' data-target='#editAccountModal'>Edit</a></td> \n\
            <td><a data-account-id='" + data.id + "' class='delete-link'>Delete</a></td> \n\
        </tr> ";

    }



});

