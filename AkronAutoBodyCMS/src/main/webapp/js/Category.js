$(document).ready(function () {
    $(document).on('submit', "#create-form", function (e) {
        e.preventDefault();

        $('#add-category-validation-errors').empty();

        var categoryData = JSON.stringify({
            category: $('#add-name').val(),
        });

        $.ajax({
            type: 'POST',
            url: contextRoot + "/category",
            data: categoryData,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            }
        }).success(function (data, status) {
            $('#add-name').val("");
            $('#admin').val("");
            $('#addCategoryModal').modal('hide');

            var tableRow = buildCategoryRow(data);
            $('#CategoryTable').append($(tableRow));
        }).error(function (data, status) {

            var errors = data.responseJSON.errors;

            $.each(errors, function (index, validationError) {
                $('#add-category-validation-errors').append(validationError.fieldName + ": " + validationError.message).append("<br/>");
            });
        });
    });

    $('#showCategoryModal').on('show.bs.modal', function (e) {

        var link = $(e.relatedTarget);

        var categoryId = link.data('category-id');

        var modal = $(this);

        $.ajax({
            type: 'GET',
            url: contextRoot + "/category/" + categoryId,
            datatype: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (category, status) {
            $('#category-name').text(category.category);
        });
    });

    $('#editCategoryModal').on('show.bs.modal', function (e) {

        var link = $(e.relatedTarget);

        var categoryId = link.data('category-id');

        var modal = $(this);

        $.ajax({
            type: 'GET',
            url: contextRoot + "/category/" + categoryId,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (category, status) {

            $('#edit-name').val(category.category);

            $('#edit-id').val(category.id);

        });
    });

    $(document).on('click', '#edit-category-button', function (e) {

        e.preventDefault();

        var categoryId = $('#edit-id').val();
        var categoryData = JSON.stringify({
            category: $('#edit-name').val(),
        });

        $.ajax({
            type: 'PUT',
            url: contextRoot + "/category/" + categoryId,
            dataType: 'json',
            data: categoryData,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            }
        }).success(function (data, status) {

            $('#editCategoryModal').modal('hide');

            var tableRow = buildCategoryRow(data);

            $('#categorys-row-' + data.id).replaceWith($(tableRow));


        });





    });
    $(document).on('click', '.delete-link', function (e) {
        e.preventDefault();
        var categoryId = $(e.target).data('category-id');

        $.ajax({
            type: 'GET',
            url: contextRoot + "/category/isFree/" + categoryId,
            datatype: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (data, status) {
            if (data) {
                $.ajax({
                    type: 'DELETE',
                    url: contextRoot + "/category/" + categoryId,
                }).success(function (data, status) {
                    $('#categorys-row-' + categoryId).remove();
                });
            } else {
                alert("A category cannot be removed if it is a blog's type...");
            }
        });





    });



    function buildCategoryRow(data) {
        return    "<tr id='categorys-row-" + data.id + "'> \n\
            <td><a data-category-id ='" + data.id + "' class = 'viewCategoryLink' data-toggle='modal' data-target='#showCategoryModal'>" + data.category + "</a></td> \n\
            <td><a data-category-id ='" + data.id + "' class = 'editCategoryLink' data-toggle='modal' data-target='#editCategoryModal'>Edit</a></td> \n\
            <td><a data-category-id='" + data.id + "' class='delete-link'>Delete</a></td> \n\
        </tr> ";

    }



});

