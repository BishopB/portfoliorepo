/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function (e) {
    $(document).on('click', '#dropDownButton', function (e) {

        var hasAccess = 'none';

        $.ajax({
            type: 'GET',
            url: contextRoot + "/userId",
            datatype: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (data, status) {
            $.each(data.authorities, function (key, each) {
                if (each === "ROLE_ADMIN") {
                    hasAccess = 'full';
                } else if (each === "ROLE_SUB_ADMIN" && hasAccess !== 'full') {
                    hasAccess = 'partial';
                }
            });
            if (hasAccess === 'full') {
                $('.modeToggle').remove();
                $("#dropDownMenu")
                        .append('<li class = "modeToggle"><a  href="' + contextRoot + '/blog" ">Blog Mode</a></li>');
                $("#dropDownMenu")
                        .append('<li class = "modeToggle"><a  href="' + contextRoot + '/staticPage" ">StaticPage Mode</a></li>');
                $("#dropDownMenu")
                        .append('<li class = "modeToggle"><a  href="' + contextRoot + '/account" ">Account Mode</a></li>');
                $("#dropDownMenu")
                        .append('<li class = "modeToggle"><a  href="' + contextRoot + '/quote" ">Quote Mode</a></li>');
                $("#dropDownMenu")
                        .append('<li class = "modeToggle"><a  href="' + contextRoot + '/category" ">Category Mode</a></li>');
            } else if (hasAccess === 'partial') {
                $('.modeToggle').remove();
                $("#dropDownMenu")
                        .append('<li class = "modeToggle"><a  href="' + contextRoot + '/blog" ">Blog Mode</a></li>');
            }
            console.log('Access: ' + hasAccess);

        });
    });
});