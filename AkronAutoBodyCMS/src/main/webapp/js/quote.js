/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    $(document).on('click', '#edit-quote-file-remove-link', function (e) {
        e.preventDefault();
        $(".edit-quote-file-link").remove();
        $(".edit-quote-file-remove").remove();
        $('#edit-quote-downloadLink-hidden').val('removed');
    });

    $(document).on('click', '.delete-link', function (e) {
        e.preventDefault();

        var quoteId = $(e.target).data('quote-id');

        $.ajax({
            type: 'DELETE',
            url: contextRoot + "/quote/" + quoteId,
        }).success(function (data, status) {
            $('#quote-row-' + quoteId).remove();
        });
    });

    $('#edit-quote-submit').on('click', function (e) {
        e.preventDefault();

        console.log("@ Quote Modal Submission Stage: 0/5");

        var imgFile = document.getElementById('edit-quote-file');
        var imgfileList = imgFile.files;

        console.log("@ Quote Modal Submission Stage: 1/5");

        var formdata = new FormData();
        if (imgfileList && imgfileList !== null && imgfileList.length > 0) {
            formdata.append("userFile", imgfileList[0]);
        }

        console.log("@ Quote Modal Submission Stage: 2/5");

        var inputTagData = $('#edit-quote-form-data :input').serializeArray();
        $.each(inputTagData, function (key, input) {
            formdata.append(input.name, input.value);
        });

        console.log("@ Quote Modal Submission Stage: 3/5");
        formdata.append('year', $('#edit-quote-year').val());
        formdata.append('make', $('#edit-quote-make').val());
        formdata.append('model', $('#edit-quote-model').val());
        console.log("@ Quote Modal Submission Stage: 4/5");


        $.ajax({
            type: 'POST',
            url: contextRoot + "/quote/modalUploadObject/update",
            data: formdata,
            processData: false,
            contentType: false

        }).success(function (data, status) {
            resetQuoteModal();
            document.getElementById("edit-quote-year").disabled = true;
            $("#edit-quote-submission-span").attr("style", "color: green; font-size: large;");
            $("#edit-quote-submission-span").text("Success ").after("<span style='color: green;' class='glyphicon glyphicon-ok editQuoteModalStatus' />");
            setTimeout(function () {
                $("#editQuoteModal").modal("hide");
            }, 1000);
            $('#quote-row-' + data.id).replaceWith(buildQuoteRow(data));
            console.log("@ Quote Modal Submission Stage: 5/5 SUCCESS");
        }).error(function (data, status) {
            $("#edit-quote-submission-span").attr("style", "color: red;");
            $("#edit-quote-submission-span").text("Failed ").after("<span style='color: red; font-size: large;' class='glyphicon glyphicon-remove editQuoteModalStatus' />");
            console.log("@ Quote Modal Submission Stage: 5/5 FAIL");
        });

    });

    $('#edit-quote-year-reset').on('click', function (e) {
        $("#edit-quote-year").empty();
        $("#edit-quote-make").empty();
        $("#edit-quote-model").empty();
        $("#edit-quote-year").append($('<option value="" disabled selected>Please select a year before continuing</option>'));
        $('#edit-quote-make').val('');
        $('#edit-quote-model').val('');
        document.getElementById("edit-quote-year").disabled = false;
        freezeEditModal();

        console.log("@ EDIT Quote Modal REVISION Stage 1");


        $.ajax({
            type: 'GET',
            url: contextRoot + "/quote/modalInfo",
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (QuoteModalInfo, status) {
            $.each(QuoteModalInfo.years, function (key, year) {
                $("#edit-quote-year").append($('<option></option>').val(year).html(year));
            });
            console.log("@ EDIT Quote Modal REVISION Stage 1 - SUCCESS");
        }).error(function (data, status) {
            console.log("@ EDIT Quote Modal REVISION Stage 1 - FAILED");
        });
    });

    $('#edit-quote-make-reset').on('click', function (e) {
        $("#edit-quote-make").empty();
        $("#edit-quote-model").empty();
        $("#edit-quote-make").append($('<option value="" disabled selected>Please select a make before continuing</option>'));
        $('#edit-quote-make').val('');
        $('#edit-quote-model').val('');
        document.getElementById("edit-quote-make").disabled = false;
        freezeEditModal();

        console.log("@ EDIT Quote Modal REVISION Stage 2");

        var info = JSON.stringify({
            selectedYear: $('#edit-quote-year').val()
        });

        $.ajax({
            type: 'POST',
            url: contextRoot + "/quote/modalInfo",
            data: info,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            }
        }).success(function (QuoteModalInfo, status) {
            $.each(QuoteModalInfo.makes, function (key, make) {
                $("#edit-quote-make").append($('<option></option>').val(make).html(make));
            });
            console.log("@ Quote Modal REVISION Stage 2 - SUCCESS");
        }).error(function (data, status) {
            console.log("@ Quote Modal REVISION Stage 2 - FAILED");
        });
    });

    $('#edit-quote-model-reset').on('click', function (e) {
        $("#edit-quote-model").empty();
        $("#edit-quote-model").append($('<option value="" disabled selected>Please select a model before continuing</option>'));
        $('#edit-quote-model').val('');
        document.getElementById("edit-quote-model").disabled = false;
        freezeEditModal();

        console.log("@ EDIT Quote Modal REVISION Stage 3");

        var info = JSON.stringify({
            selectedYear: $('#edit-quote-year').val(),
            selectedMake: $('#edit-quote-make').val()
        });

        $.ajax({
            type: 'POST',
            url: contextRoot + "/quote/modalInfo",
            data: info,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            }
        }).success(function (QuoteModalInfo, status) {
            $.each(QuoteModalInfo.models, function (key, model) {
                $("#edit-quote-model").append($('<option></option>').val(model).html(model));
            });
            console.log("@ Quote Modal REVISION Stage 3 - SUCCESS");
        }).error(function (data, status) {
            console.log("@ Quote Modal REVISION Stage 3 - FAILED");
        });
    });

    $('#edit-quote-year').on('change', function () {
        document.getElementById("edit-quote-year").disabled = true;
        document.getElementById("edit-quote-make").disabled = false;

        $("#edit-quote-make").append($('<option value="" disabled selected>Please select a make before continuing</option>'));

        console.log("@ EDIT Quote Modal Stage 2");

        var info = JSON.stringify({
            selectedYear: $('#edit-quote-year').val()
        });

        $.ajax({
            type: 'POST',
            url: contextRoot + "/quote/modalInfo",
            data: info,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            }
        }).success(function (QuoteModalInfo, status) {
            $.each(QuoteModalInfo.makes, function (key, make) {
                $("#edit-quote-make").append($('<option></option>').val(make).html(make));
            });
            console.log("@ EDIT Quote Modal Stage 2 - SUCCESS");
        }).error(function (data, status) {
            console.log("@ EDIT Quote Modal Stage 2 - FAILED");
        });
    });

    $('#edit-quote-make').on('change', function () {
        document.getElementById("edit-quote-make").disabled = true;
        document.getElementById("edit-quote-model").disabled = false;

        $("#edit-quote-model").append($('<option value="" disabled selected>Please select a model before continuing</option>'));

        console.log("@ Quote Modal Stage 3");

        var info = JSON.stringify({
            selectedYear: $('#edit-quote-year').val(),
            selectedMake: $('#edit-quote-make').val()
        });

        $.ajax({
            type: 'POST',
            url: contextRoot + "/quote/modalInfo",
            data: info,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            }
        }).success(function (QuoteModalInfo, status) {
            $.each(QuoteModalInfo.models, function (key, model) {
                $("#edit-quote-model").append($('<option></option>').val(model).html(model));
            });
            console.log("@ EDIT Quote Modal Stage 3 - SUCCESS");
        }).error(function (data, status) {
            console.log("@ EDIT Quote Modal Stage 3 - FAILED");
        });
    });

    $('#edit-quote-model').on('change', function () {
        document.getElementById("edit-quote-model").disabled = true;
        document.getElementById("edit-quote-body-style").disabled = false;
        document.getElementById("edit-quote-vin").disabled = false;
        document.getElementById("edit-quote-description").disabled = false;
        document.getElementById("edit-quote-file").disabled = false;
        document.getElementById("edit-quote-email").disabled = false;
        document.getElementById("edit-quote-phone").disabled = false;
        document.getElementById("edit-quote-name").disabled = false;
        document.getElementById("edit-quote-submit").disabled = false;

        console.log("@ EDIT Quote Modal Stage 4 (success)");
    });

    $('#editQuoteModal').on('show.bs.modal', function (e) {
        $('#edit-quote-year').val('');
        $('#edit-quote-make').val('');
        $('#edit-quote-model').val('');
        $('#edit-quote-body-style').val('');
        $('#edit-quote-vin').val('');
        $('#edit-quote-description').val('');
        $('#edit-quote-file').val('');
        $(".edit-quote-file-link").remove();
        $('#edit-quote-email').val('');
        $('#edit-quote-phone').val('');
        $('#edit-quote-name').val('');
        $('#edit-quote-submit').val('');

        resetEditModal();

        var link = $(e.relatedTarget);
        var quoteId = link.data('quote-id');
        var modal = $(this);

        $('#edit-quote-id').val(quoteId);

        console.log("@ EDIT Quote Modal Stage 1");

        $.ajax({
            type: 'GET',
            url: contextRoot + "/quote/show/" + quoteId,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (quote, status) {
            modal.find('#edit-quote-year').append($('<option value="' + quote.year + '" selected>' + quote.year + '</option>'));
            modal.find('#edit-quote-make').append($('<option value="' + quote.make + '" selected>' + quote.make + '</option>'));
            modal.find('#edit-quote-model').append($('<option value="' + quote.model + '" selected>' + quote.model + '</option>'));
            modal.find('#edit-quote-body-style').val(quote.bodyStyle);
            modal.find('#edit-quote-vin').val(quote.vin);
            modal.find('#edit-quote-description').val(quote.description);
            if (quote.downloadLink.length > 5) {
                $('#edit-quote-file-div').after("<div class='col-md-7 text-center'><a class='edit-quote-file-link' href='" + quote.downloadLink + "'>Download Last File</a><a id='edit-quote-file-remove-link' class='edit-quote-file-remove' href=''><br />Remove Last File</a></div>");
            } else {
                $('#edit-quote-file-div').after("<div class='col-md-7 text-center'><span class='edit-quote-file-link'>No Previous File</span></div>");
            }
            modal.find('#edit-quote-email').val(quote.email);
            modal.find('#edit-quote-phone').val(quote.phone);
            modal.find('#edit-quote-name').val(quote.name);

            console.log("@ EDIT Quote Modal Stage 1 - SUCCESS");
        }).error(function (data, status) {
            console.log("@ EDIT Quote Modal Stage 1 - FAILED");
        });
    });

    $('#showQuoteModal').on('show.bs.modal', function (e) {
        $('#show-quote-year').val('');
        $('#show-quote-make').val('');
        $('#show-quote-model').val('');
        $('#show-quote-body-style').val('');
        $('#show-quote-vin').val('');
        $('#show-quote-description').val('');
        $(".show-quote-file-link").remove();
        $('#show-quote-email').val('');
        $('#show-quote-phone').val('');
        $('#show-quote-name').val('');
        $('#show-quote-submit').val('');

        var link = $(e.relatedTarget);
        var quoteId = link.data('quote-id');
        var modal = $(this);

        console.log("@ SHOW Quote Modal Stage 1");

        $.ajax({
            type: 'GET',
            url: contextRoot + "/quote/show/" + quoteId,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (quote, status) {
            modal.find('#show-quote-year').val(quote.year);
            modal.find('#show-quote-make').val(quote.make);
            modal.find('#show-quote-model').val(quote.model);
            modal.find('#show-quote-body-style').val(quote.bodyStyle);
            modal.find('#show-quote-vin').val(quote.vin);
            modal.find('#show-quote-description').val(quote.description);
            if (quote.downloadLink.length > 1) {
                $('#show-quote-file').after("<a class='show-quote-file-link' href='" + quote.downloadLink + "'>Download</a>");
            } else {
                $('#show-quote-file').after("<span class='show-quote-file-link'>No File to Download</span>");
            }
            modal.find('#show-quote-email').val(quote.email);
            modal.find('#show-quote-phone').val(quote.phone);
            modal.find('#show-quote-name').val(quote.name);

            console.log("@ SHOW Quote Modal Stage 1 - SUCCESS");
        }).error(function (data, status) {
            console.log("@ SHOW Quote Modal Stage 1 - FAILED");
        });
    });

    $('#getQuoteModal').on('show.bs.modal', function (e) {
        var link = $(e.relatedTarget);
        var modal = $(this);

        resetQuoteModal();

        $("#quote-year").append($('<option value="" disabled selected>Please select a year before continuing</option>'));

        console.log("@ Quote Modal Stage 1");

        $.ajax({
            type: 'GET',
            url: contextRoot + "/quote/modalInfo",
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (QuoteModalInfo, status) {
            $.each(QuoteModalInfo.years, function (key, year) {
                $("#quote-year").append($('<option></option>').val(year).html(year));
            });
            console.log("@ Quote Modal Stage 1 - SUCCESS");
        }).error(function (data, status) {
            console.log("@ Quote Modal Stage 1 - FAILED");
        });
    });

    $('#quote-year').on('change', function () {
        document.getElementById("quote-year").disabled = true;
        document.getElementById("quote-make").disabled = false;

        $("#quote-make").append($('<option value="" disabled selected>Please select a make before continuing</option>'));

        console.log("@ Quote Modal Stage 2");

        var info = JSON.stringify({
            selectedYear: $('#quote-year').val()
        });

        $.ajax({
            type: 'POST',
            url: contextRoot + "/quote/modalInfo",
            data: info,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            }
        }).success(function (QuoteModalInfo, status) {
            $.each(QuoteModalInfo.makes, function (key, make) {
                $("#quote-make").append($('<option></option>').val(make).html(make));
            });
            console.log("@ Quote Modal Stage 2 - SUCCESS");
        }).error(function (data, status) {
            console.log("@ Quote Modal Stage 2 - FAILED");
        });
    });

    $('#quote-make').on('change', function () {
        document.getElementById("quote-make").disabled = true;
        document.getElementById("quote-model").disabled = false;

        $("#quote-model").append($('<option value="" disabled selected>Please select a model before continuing</option>'));

        console.log("@ Quote Modal Stage 3");

        var info = JSON.stringify({
            selectedYear: $('#quote-year').val(),
            selectedMake: $('#quote-make').val()
        });

        $.ajax({
            type: 'POST',
            url: contextRoot + "/quote/modalInfo",
            data: info,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            }
        }).success(function (QuoteModalInfo, status) {
            $.each(QuoteModalInfo.models, function (key, model) {
                $("#quote-model").append($('<option></option>').val(model).html(model));
            });
            console.log("@ Quote Modal Stage 3 - SUCCESS");
        }).error(function (data, status) {
            console.log("@ Quote Modal Stage 3 - FAILED");
        });
    });

    $('#quote-model').on('change', function () {
        document.getElementById("quote-model").disabled = true;
        document.getElementById("quote-body-style").disabled = false;
        document.getElementById("quote-vin").disabled = false;
        document.getElementById("quote-description").disabled = false;
        document.getElementById("quote-upload").disabled = false;
        document.getElementById("quote-email").disabled = false;
        document.getElementById("quote-phone").disabled = false;
        document.getElementById("quote-name").disabled = false;
        document.getElementById("quote-submit").disabled = false;

        console.log("@ Quote Modal Stage 4 (success)");
    });

    $('#quote-submit').on('click', function (e) {
        e.preventDefault();

        console.log("@ Quote Modal Submission Stage: 0/5");

        var imgFile = document.getElementById('quote-upload');
        var imgfileList = imgFile.files;

        console.log("@ Quote Modal Submission Stage: 1/5");

        var formdata = new FormData();
        if (imgfileList && imgfileList !== null && imgfileList.length > 0) {
            formdata.append("userFile", imgfileList[0]);
        }

        console.log("@ Quote Modal Submission Stage: 2/5");

        var inputTagData = $('#quote-form-data :input').serializeArray();
        $.each(inputTagData, function (key, input) {
            formdata.append(input.name, input.value);
        });

        console.log("@ Quote Modal Submission Stage: 3/5");
        formdata.append('year', $('#quote-year').val());
        formdata.append('make', $('#quote-make').val());
        formdata.append('model', $('#quote-model').val());

        console.log("@ Quote Modal Submission Stage: 4/5");


        $.ajax({
            type: 'POST',
            url: contextRoot + "/quote/modalUploadObject",
            data: formdata,
            processData: false,
            contentType: false

        }).success(function (data, status) {
            resetQuoteModal();
            document.getElementById("quote-year").disabled = true;
            $("#quote-submission-span").attr("style", "color: green; font-size: large;");
            $("#quote-submission-span").text("Success ").after("<span style='color: green;' class='glyphicon glyphicon-ok quoteModalStatus' />");
            setTimeout(function () {
                $("#getQuoteModal").modal("hide");
            }, 1000);
            $('#QuoteTable').append(buildQuoteRow(data));
            console.log("@ Quote Modal Submission Stage: 5/5 SUCCESS");
        }).error(function (data, status) {
            $("#quote-submission-span").attr("style", "color: red;");
            $("#quote-submission-span").text("Failed ").after("<span style='color: red; font-size: large;' class='glyphicon glyphicon-remove quoteModalStatus' />");
            console.log("@ Quote Modal Submission Stage: 5/5 FAIL");
        });
    });

    function freezeEditModal() {
        document.getElementById("edit-quote-body-style").disabled = true;
        document.getElementById("edit-quote-vin").disabled = true;
        document.getElementById("edit-quote-description").disabled = true;
        document.getElementById("edit-quote-file").disabled = true;
        document.getElementById("edit-quote-email").disabled = true;
        document.getElementById("edit-quote-phone").disabled = true;
        document.getElementById("edit-quote-name").disabled = true;
        document.getElementById("edit-quote-submit").disabled = true;
    }
    function resetEditModal() {
        document.getElementById("edit-quote-year").disabled = true;
        document.getElementById("edit-quote-make").disabled = true;
        document.getElementById("edit-quote-model").disabled = true;
        document.getElementById("edit-quote-body-style").disabled = false;
        document.getElementById("edit-quote-vin").disabled = false;
        document.getElementById("edit-quote-description").disabled = false;
        document.getElementById("edit-quote-file").disabled = false;
        document.getElementById("edit-quote-email").disabled = false;
        document.getElementById("edit-quote-phone").disabled = false;
        document.getElementById("edit-quote-name").disabled = false;
        document.getElementById("edit-quote-submit").disabled = false;
        $("#edit-quote-submission-span").text('');
        $(".editQuoteModalStatus").remove();
        $(".edit-quote-file-link").remove();
        $(".edit-quote-file-remove").remove();

    }

    function resetQuoteModal() {
        $("#quote-year").empty();
        $("#quote-make").empty();
        $("#quote-model").empty();
        $("#quote-submission-span").text('');
        $(".quoteModalStatus").remove();

        $('#quote-year').val('');
        $('#quote-make').val('');
        $('#quote-model').val('');
        $('#quote-body-style').val('');
        $('#quote-vin').val('');
        $('#quote-description').val('');
        $('#quote-upload').val('');
        $('#quote-email').val('');
        $('#quote-phone').val('');
        $('#quote-name').val('');
        $('#quote-submit').val('');

        document.getElementById("quote-year").disabled = false;
        document.getElementById("quote-make").disabled = true;
        document.getElementById("quote-model").disabled = true;
        document.getElementById("quote-body-style").disabled = true;
        document.getElementById("quote-vin").disabled = true;
        document.getElementById("quote-description").disabled = true;
        document.getElementById("quote-upload").disabled = true;
        document.getElementById("quote-email").disabled = true;
        document.getElementById("quote-phone").disabled = true;
        document.getElementById("quote-name").disabled = true;
        document.getElementById("quote-submit").disabled = true;
    }
    function buildQuoteRow(data) {
        return    "<tr id='quote-row-" + data.id + "'> \n\
            <td><a data-quote-id ='" + data.id + "' class = 'viewQuoteLink' data-toggle='modal' data-target='#showQuoteModal'>" + data.email + "</a></td> \n\
            <td><a data-quote-id ='" + data.id + "' class = 'editQuoteLink' data-toggle='modal' data-target='#editQuoteModal'>Edit</a></td> \n\
            <td><a data-quote-id='" + data.id + "' class='delete-link'>Delete</a></td> \n\
        </tr> ";

    }
});