/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    var editIdExchange;
    $(document).on("click", "#add-blog-button", function (e) {
        e.preventDefault();
        $('#add-button-holder').hide();
        $('.editHiddenRow').hide();
        $('#add-form').show();
        $(document).scrollTop($("#add-form").offset().top);
    });

    $(document).on("click", "#cancel-button", function (e) {
        resetToInitialView();
    });

    $(document).on("click", "#submitter", function (e) {
        e.preventDefault();
        $.ajax({
            type: 'GET',
            url: contextRoot + "/userId",
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            }
        }).success(function (data, status) {
            var staticPageObject = JSON.stringify({
                title: $("#add-title").val(),
                content: tinymce.get('mytextarea').getContent(),
                accountId: data.id
            });

            $.ajax({
                type: 'POST',
                url: contextRoot + '/staticPage',
                data: staticPageObject,
                dataType: 'json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-type", "application/json");
                }
            }).success(function (data, status) {
                var tableRow = tableRowBuilder(data);
                $('#AdminBlogsTable').append($(tableRow));

                resetToInitialView();
            }).error(function (data, status) {
                errorHandler(data);
            });

        }).error(function (data, success) {
            alert("You Don't Belong Here!!!");
        });
    });

    $(document).on("click", ".viewBlogLink", function (e) {
        e.preventDefault();
        resetToInitialView();
        var link = $(e.target);
        var pageId = link.data('page-id');
        $('#add-button-holder').hide();
        $('#contentView').empty();


        $.ajax({
            type: 'GET',
            url: contextRoot + "/staticPage/" + pageId,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            }
        }).success(function (data, success) {
            $('#showStaticPageModal').modal('toggle');
            $('.modal-title').text(data.title);
            $('#contentView').append(data.content);
        });
    });

    $('#showStaticPageModal').on('hide.bs.modal', function (e) {
        $('#add-button-holder').show();
    });

    $(document).on("click", ".deleteBlogLink", function (e) {
        var link = $(e.target);
        var pageId = link.data('page-id');

        $.ajax({
            type: 'DELETE',
            url: contextRoot + '/staticPage/' + pageId
        }).success(function (data, success) {
            $('#static-page-row-' + pageId).remove();
            $('#edit-row-' + pageId).remove();
        });

    });

    $(document).on("click", '.editBlogLink', function (e) {
        e.preventDefault();
        resetToInitialView();
        $('#add-button-holder').hide();
        var tinyMceDiv = $("#add-form").clone().prop('id', 'removeMeLater');
        var link = $(e.target);
        var editTextArea = editTinyMceBuilder();
        var pageId = link.data('page-id');

//      remove row with old textarea
        $(tinyMceDiv).find("#textarea-row").remove();
        //put new generated textArea where removal was. Before the buttons
        $(tinyMceDiv).find("#bottom-buttons").before(editTextArea);
        //change id of submit button to trigger different event
        $(tinyMceDiv).find('#submitter').prop('id', 'editter');
//        $('td div div div form #primaryKey').val(blogId);
        editIdExchange = pageId;

        $('#editor-cell-' + pageId).append(tinyMceDiv);


        $.ajax({
            type: 'GET',
            url: contextRoot + "/staticPage/" + pageId,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            }

        }).success(function (data, status) {
            //initialize new textarea as an instance of edit-text-area
            tinymce.init({
                selector: '#edit-text-area',
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste jbimages"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
                relative_urls: false
            });
            $('#edit-row-' + pageId).show();
            $('td #removeMeLater').show();

            tinymce.get('edit-text-area').setContent(data.content);
            $("#add-title").val(data.title);
        });
    });

    $(document).on("click", "#editter", function (e) {

        e.preventDefault();
        var pageId = editIdExchange;
        tinymce.triggerSave();
        $.ajax({
            type: 'GET',
            url: contextRoot + "/userId",
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            }
        }).success(function (data, status) {
            var pageObject = JSON.stringify({
                title: $("#add-title").val(),
                content: tinymce.get('edit-text-area').getContent(),
                accountId: data.id
            });
            $.ajax({
                type: 'PUT',
                url: contextRoot + "/staticPage/" + pageId,
                data: pageObject,
                dataType: 'json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-type", "application/json");
                }
            }).success(function (data, status) {
                resetToInitialView();
                var newTitleCell = titleTableCell(data);
                $('#view-' + data.id).replaceWith(newTitleCell);
            }).error(function (data, status) {
                errorHandler(data);
            });
        }).error(function(data, status){
            alert("Mama didn't raise no snitch!");
        });
    });

    function resetToInitialView() {
//      Add button        
        $('#add-button-holder').show();
//      reset hidden form values  
        $('#add-start-date').val("");
        $('#add-end-date').val("");
        $('#add-title').val("");
        $('#add-form').hide();
        tinymce.get('mytextarea').setContent('Enter new content...');

        //clean up accordian table
        $('td #removeMeLater').hide();
        $('#removeMeLater').remove();
        tinymce.EditorManager.execCommand('mceRemoveEditor', true, 'edit-text-area');
        $('.editHiddenRow').hide();
        $('#errorBody').empty();

    }



    function tableRowBuilder(data) {
        return '<tr id = "static-page-row-' + data.id + '">\n\
        <td><a data-page-id ="' + data.id + '"id ="view-' + data.id + '" class = "viewBlogLink">' + data.title + '</a></td>\n\
        <td><a data-page-id="' + data.id + '" class = "editBlogLink">Edit</a></td>\n\
        <td><a data-page-id="' + data.id + '"class="deleteBlogLink">Delete</a></td>\n\
        </tr>\n\
        <tr style="display:none" id="edit-row-' + data.id + '" class = "editHiddenRow">\n\
        <td colspon="3"><div id="editor-cell-' + data.id + '"></div></td>\n\
        </tr>';
    }

    function editTinyMceBuilder() {
        return '<div id="textarea-row" class="row">\n\
            <textarea id="edit-text-area"></textarea>\n\
            </div>';
    }

    function titleTableCell(data) {
        return '<td><a data-page-id ="' + data.id + '" id="view-' + data.id + '"class = "viewBlogLink">' + data.title + '</a></td>';
    }

    function errorHandler(data) {
        $('.errorMsg').remove();

        var error = data.responseJSON.errors;

        $.each(error, function (index, validationError) {
//                $('#error-'+validationError.fieldName).append(validationError.fieldName + ": " + validationError.message).append("<br />");
            $('#errorBody').append("<div class = 'errorMsg'><strong>Input Error!:</strong> " + validationError.message + "</div><br/>");

            if (validationError.fieldName == 'title') {
                $('#add-title').attr("style", "border:3px solid red;");
            }
        });

        $('#errorModal').modal('toggle');
    }
});

