/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function (e) {
    $('.modeToggle').remove();
    $(document).on("click", "#admin-login", function (e) {
        e.preventDefault();
        $(location).attr('href', 'http://localhost:8080/AkronAutoBodyCMS/blog');
//        $(location).attr('href', 'http://localhost:8080/AkronAutoBodyCMS/login');

    });

    $(document).on("click", ".staticPageLink", function (e) {
        var link = $(e.target);

        var staticPageId = link.data('staticpage-id');

        $.ajax({
            type: 'GET',
            url: contextRoot + '/' + staticPageId,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (data, status) {
            $('#blogList').empty();
            $('#blogList').append('<div style="background-color: whitesmoke;padding: 50px; 50px; border-radius: 15px;">' + data.content + '</div>');
        });
    });

    $(document).on("click", "#dropDownButton", function (e) {
        e.preventDefault();
        $('li .staticPageLink').remove();
//        $('li .modeToggle').remove();
        $.ajax({
            type: 'GET',
            url: contextRoot + '/dropdown',
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            }
        }).success(function (data, status) {
            $.each(data, function (key, value) {
                $("#dropDownMenu")
                        .append('<li><a data-staticpage-id = "' + value.id + '" class = "staticPageLink ">' + value.title + '</a></li>');
            });
        });
    });

});


