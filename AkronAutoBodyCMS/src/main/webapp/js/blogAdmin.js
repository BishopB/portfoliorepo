/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    var editIdExchange;

    $(document).on("click", "#submitter", function (e) {
        e.preventDefault();
        $.ajax({
            type: 'GET',
            url: contextRoot + "/userId",
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            }
        }).success(function (data, status) {

            e.preventDefault();
            var authority = authorities(data);

            var blogObject = JSON.stringify({
                startDate: $("#add-start-date").val(),
                endDate: $("#add-end-date").val(),
                title: $("#add-title").val(),
                categoryId: $("#add-category").val(),
                content: tinymce.get('mytextarea').getContent(),
                accountId: data.id,
                approved: authority
            });
            $.ajax({
                type: 'POST',
                url: contextRoot + "/blog",
                data: blogObject,
                dataType: 'json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-type", "application/json");
                }
            }).success(function (data, status) {
                var tableRow = tableRowBuilder(data);
                $('#AdminBlogsTable').append($(tableRow));
                resetToInitialView();
            }).error(function (data, status) {
                errorHandler(data);
            });
        }).error(function (data, status) {
            console.log("NOOO!@!!");
        });
    });
    //trying to trigger status modal
    $(document).on('click', '.btn-mystery', function (e) {
        e.preventDefault();
        $('#statusBlogModal').modal('toggle');

//        var accountId = link.data('account-id');
        var modal = $('#statusBlogModal');
        $.ajax({
            type: 'GET',
            url: contextRoot + "/userId",
            datatype: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (data, status) {
            $.each(data.authorities, function (key, each) {
                if (each === "ROLE_ADMIN") {
                    modal.find('.statusClass').attr('disabled', false);
                }
            });
        });
    });

    $(document).on("click", "#add-blog-button", function (e) {
        e.preventDefault();
        $('#add-button-holder').hide();
        $('.editHiddenRow').hide();
        $('#add-form').show();
        $(document).scrollTop($("#add-form").offset().top);
    });

    $(document).on("click", "#cancel-button", function (e) {
        e.preventDefault();
        resetToInitialView();
    });

    $(document).on("click", '.editBlogLink', function (e) {
        e.preventDefault();
        resetToInitialView();
        $.ajax({
            type: 'GET',
            url: contextRoot + "/userId",
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            }
        }).success(function (data, status) {

            var access = authorities(data);

            if (access == "false") {
                alert("You Don't Have Access To That!!!");
            } else {

                $('#add-button-holder').hide();
                var tinyMceDiv = $("#add-form").clone().prop('id', 'removeMeLater');
                var link = $(e.target);
                var editTextArea = editTinyMceBuilder();
                var blogId = link.data('blog-id');
                var statusButton = buttonGenerator();
//      remove row with old textarea
                $(tinyMceDiv).find("#textarea-row").remove();
                //put new generated textArea where removal was. Before the buttons
                $(tinyMceDiv).find("#bottom-buttons").before(editTextArea);
                $(tinyMceDiv).find('#status-button-holder').append(statusButton);
                //change id of submit button to trigger different event
                $(tinyMceDiv).find('#submitter').prop('id', 'editter');
//        $('td div div div form #primaryKey').val(blogId);
                editIdExchange = blogId;
                $('#editor-cell-' + blogId).append(tinyMceDiv);
                $.ajax({
                    type: 'GET',
                    url: contextRoot + "/blog/read/" + blogId,
                    dataType: 'json',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-type", "application/json");
                    }

                }).success(function (data, status) {
//initialize new textarea as an instance of edit-text-area
//                    tinymce.init({
//                        selector: '#edit-text-area',
//                        plugins: ["image"],
//                        image_list: [
//                            {title: 'My image 1', value: '${pageContext.request.contextPath}/uploadFile/showImage/32'},
//                            {title: 'My image 2', value: 'http://www.moxiecode.com/my2.gif'}
//                        ]
//                    });
                    tinymce.init({
                        selector: "#edit-text-area",
                        plugins: [
                            "advlist autolink lists link image charmap print preview anchor",
                            "searchreplace visualblocks code fullscreen",
                            "insertdatetime media table contextmenu paste jbimages"
                        ],
                        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
                        relative_urls: false
                    });
                    $('#edit-row-' + blogId).show();
                    $('td #removeMeLater').show();
                    tinymce.get('edit-text-area').setContent(data.content);
                    $("#add-start-date").val(data.startDate);
                    $("#add-end-date").val(data.endDate);
                    $("#add-title").val(data.title);
                    $('#add-category').val(data.categoryId);
                    (data.approval === true ? $('#status-approved-blog-button').prop("checked", "checked") : $('#status-pending-blog-button').prop("checked", "checked"));

                });
            }
        });
    });

    $(document).on("click", "#editter", function (e) {

        e.preventDefault();
        $.ajax({
            type: 'GET',
            url: contextRoot + "/userId",
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            }
        }).success(function (data, status) {
            var blogId = editIdExchange;
            tinymce.triggerSave();
            var authority = authorities(data);
//        var bogId = $('td div div div form #primaryKey').val();
            console.log($('.statusClass:checked').val());
            var blogObject = JSON.stringify({
                startDate: $("#add-start-date").val(),
                endDate: $("#add-end-date").val(),
                title: $("#add-title").val(),
                categoryId: $("#add-category").val(),
                content: tinymce.get('edit-text-area').getContent(),
                accountId: data.id,
                approved: $('.statusClass:checked').val()
            });
            $.ajax({
                type: 'PUT',
                url: contextRoot + "/blog/" + blogId,
                data: blogObject,
                dataType: 'json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-type", "application/json");
                }
            }).success(function (data, status) {
                resetToInitialView();
                var newTitleCell = titleTableCell(data);
                $('#view-' + data.id).replaceWith(newTitleCell);
            }).error(function (data, status) {
                errorHandler(data);
            });
        }).error(function (data, status) {
            alert("I HATE THIS!!!! ... but its gon b kay! :)");
        });
    });

    $(document).on("click", ".deleteBlogLink", function (e) {
        var link = $(e.target);
        var blogId = link.data('blog-id');
        $.ajax({
            type: 'DELETE',
            url: contextRoot + '/blog/' + blogId
        }).success(function (data, success) {
            $('#blogs-row-' + blogId).remove();
            $('#edit-row-' + blogId).remove();
        });
    });

    $(document).on("click", ".viewBlogLink", function (e) {
        e.preventDefault();
        resetToInitialView();
        var link = $(e.target);
        var blogId = link.data('blog-id');
        $('#add-button-holder').hide();
        $('#contentView').empty();
        $.ajax({
            type: 'GET',
            url: contextRoot + "/blog/read/" + blogId,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            }
        }).success(function (data, success) {
            $('#showBlogModal').modal('toggle');
            $('.modal-title').text(data.title);
            $('#contentView').append(data.content);
            $('#showFooter').append(status(data.approval));
        });
    });

    $('#showBlogModal').on('hide.bs.modal', function (e) {
        $('#add-button-holder').show();
    });

    function tableRowBuilder(data) {
        return '<tr id = "blogs-row-' + data.id + '">\n\
        <td><a data-blog-id ="' + data.id + '"id ="view-' + data.id + '" class = "viewBlogLink">' + data.title + '</a></td>\n\
        <td><a data-blog-id="' + data.id + '" class = "editBlogLink">Edit</a></td>\n\
        <td><a data-blog-id="' + data.id + '"class="deleteBlogLink">Delete</a></td>\n\
        </tr>\n\
        <tr style="display:none" id="edit-row-' + data.id + '" class = "editHiddenRow">\n\
        <td colspon="3"><div id="editor-cell-' + data.id + '"></div></td>\n\
        </tr>';
    }

    function editTinyMceBuilder() {
        return '<div id="textarea-row" class="row">\n\
            <textarea id="edit-text-area"></textarea>\n\
            </div>';
    }

    function resetToInitialView() {
//      Add button        
        $('#add-button-holder').show();
//      reset hidden form values  
        $('#add-start-date').val("");
        $('#add-end-date').val("");
        $('#add-title').val("");
        $('#add-form').hide();
        tinymce.get('mytextarea').setContent('Enter new content...');
        //clean up accordian table
        $('td #removeMeLater').hide();
        $('#removeMeLater').remove();
        tinymce.EditorManager.execCommand('mceRemoveEditor', true, 'edit-text-area');
        $('.editHiddenRow').hide();
        //clean up error stuff
        $('#add-title').removeAttr('style');
        $('#add-start-date').removeAttr('style');
        $('#add-end-date').removeAttr('style');
        $('#errorBody').empty();
        $('.glyphicon-exclamation-sign').remove();
        $('.removable').remove();
        $('.glyphicon-ok').remove();
    }

    function titleTableCell(data) {
        return '<td><a data-blog-id ="' + data.id + '" id="view-' + data.id + '"class = "viewBlogLink">' + data.title + '</a></td>';
    }

    function errorHandler(data) {
        $('.errorMsg').remove();
        var error = data.responseJSON.errors;
        $.each(error, function (index, validationError) {
//                $('#error-'+validationError.fieldName).append(validationError.fieldName + ": " + validationError.message).append("<br />");
            $('#errorBody').append("<div class = 'errorMsg'><strong>Input Error!:</strong> " + validationError.message + "</div><br/>");
            if (validationError.fieldName == 'startDate') {
                $("#add-start-date").attr("style", " border:3px solid red;");
            } else if (validationError.fieldName == 'endDate') {
                $('#add-end-date').attr("style", "border: 3px solid red;");
            } else if (validationError.fieldName == 'title') {
                $('#add-title').attr("style", "border:3px solid red;");
            }
        });
        $('#errorModal').modal('toggle');
    }

    function authorities(data) {
        var hasAccess = 'none';
        $.each(data.authorities, function (key, each) {
            if (each === "ROLE_ADMIN") {
                hasAccess = 'true';
            } else if (each === "ROLE_SUB_ADMIN" && hasAccess !== 'true') {
                hasAccess = 'false';
            }
        });
        return hasAccess;
    }

    function buttonGenerator() {
        return "<strong>Approved</strong><input type = 'radio' id = 'status-approved-blog-button' class ='statusClass' name='status' value='true'/>&nbsp;&nbsp;<strong>Pending</strong><input type = 'radio' id = 'status-pending-blog-button' class ='statusClass' name='status' value='false'/>";
    }

    function status(approval) {
        if (approval == true) {
            return '<div class = "pull-left removable"><span class = "glyphicon glyphicon-ok"></span>Approved </div>';
        }
        return '<div class = "pull-left removable"><span class = "glyphicon glyphicon-exclamation-sign"></span>Pending</div> ';
    }
});