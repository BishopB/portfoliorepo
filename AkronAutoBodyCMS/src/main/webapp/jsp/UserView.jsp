<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Akron Auto Body</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">      
        <style>
            body {
            }

            .hidden{
                display:none;
            }

            .unhidden{
                display:block;
            }

            #blogtitle{
                font-size: 2.5em;
            }

            #blogdate{
                font-size: 1em;
            }

            .table {
                border-bottom:0px !important;
            }
            .table th, .table td {
                border: 0px !important;
            }
            .fixed-table-container {
                border:0px !important;
            }

            #blogList{
                padding-top: 7%;

                /*                                right:50%;
                                                left:50%;
                                float: right;*/
            }
            #UserBlogsTable{
                word-wrap: break-word;
            }

            #arrow{
                float:right;
            }
            #stick{
                position: fixed;
                bottom: 0px;
                padding-left: 2%;
            }
            #blog-content{
                word-wrap: break-word;
            }
            .footer{
                padding-top: 33%;
            }
            .table-striped > tbody > tr:nth-child(odd) > td, .table-striped > tbody > tr:nth-child(odd) > th {
                background-color: threedface;
            }
            .table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
                background-color: lightblue;
            }
        </style>
    </head>
    <body class="col-md-12" background="${pageContext.request.contextPath}/img/bkgd.jpg">
        <!--link to top for sticky footer-->
        <a name="top"></a>
        <!--link to top for sticky footer-->

        <jsp:include page="/jsp/stickyHeader.jsp" /> 


        <div id="blogList"class="container">


            <table id="UserBlogsTable" class="table table-striped" style="table-layout: fixed; width: 100%;">                 
                <c:forEach items="${UserBlogsTable}" var="blog">
                    <tr id="blog-row-${blog.id}">
                        <td id="blogtitle" width="90%">${blog.title}</td>
                        <td>${blog.startDate}</td>

                    </tr>
                    <tr>
                        <td colspan="2" id="blog-content" style="word-wrap: break-word;">${blog.content}<hr /></td>
                    </tr>
                    <tr style="background-color: transparent; height: 15px"></tr>
                    <tr></tr>
                </c:forEach>
            </table>

        </div>







        <!--beginning of fixed footer-->
        <footer class="footer ">
            <hr />
            <div class ="row">
                <div class ="col-md-4 col-md-push-4">
                    <center><h3 style="color: grey"> 528 S Main St. Akron, OH 44311 </h3></center>
                    <center><h4 style="color: grey"> (855) 600-9584 </h4></center>

                </div>
            </div>
            <br />
            <hr />
            <div class="row">
                <div class = "col-md-2">
                    <h3><a id ="admin-login" >Log In</a></h3>
                </div>
                <c:forEach items= "${StaticPageList}"  var = "staticPage">
                    <div class = "col-md-2">
                        <h4><a data-staticPage-id = "${staticPage.id}" class = "staticPageLink">${staticPage.title}</a></h4>
                    </div>
                </c:forEach>

            </div>

        </footer>
        <!--end of fixed footer-->

        <!--beginning of sticky footer: requires link at top under opening body tag-->
        <!--        <footer  class="footer navbar-fixed-bottom">-->

        <div id="stick" class="col-md-push-11 col-md-1">
            <a href="#top"><img src="http://www.free-icons-download.net/images/back-to-top-icon-33007.png"
                                alt="Back to Top" style="width:50px;height:50px;"></a>
        </div>
        <!--</footer>-->
        <!--end of sticky footer-->

        <script type="text/javascript">
            var contextRoot = "${pageContext.request.contextPath}";
        </script>


        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

        <script src="${pageContext.request.contextPath}/js/adminDropdownSelection.js"></script>
        <script src="${pageContext.request.contextPath}/js/UserView.js"></script>
        <script src="${pageContext.request.contextPath}/js/quote.js"></script>
        <script src="${pageContext.request.contextPath}/js/AdvancedSearch.js"></script>




    </body>
</html>