<%-- 
    Document   : stickyHeader
    Created on : Mar 10, 2016, 2:14:41 PM
    Author     : apprentice
--%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<style>
    #search{

        top: 8px;

    }
    #adv{
        bottom: 20px;
    }
    .center-block {
        float: none;
        margin-left: auto;
        margin-right: auto;
    }

    .input-group .icon-addon .form-control {
        border-radius: 0;
    }

    .icon-addon {
        position: relative;
        color: #555;
        display: block;
    }

    .icon-addon:after,
    .icon-addon:before {
        display: table;
        content: " ";
    }

    .icon-addon:after {
        clear: both;
    }

    .icon-addon.addon-md .glyphicon,
    .icon-addon .glyphicon, 
    .icon-addon.addon-md .fa,
    .icon-addon .fa {
        position: absolute;
        z-index: 2;
        left: 10px;
        font-size: 14px;
        width: 20px;
        margin-left: -2.5px;
        text-align: center;
        padding: 10px 0;
        top: 1px
    }


    .icon-addon.addon-md .form-control,
    .icon-addon .form-control {
        padding-left: 30px;
        float: left;
        font-weight: normal;
    }


    .icon-addon .form-control:focus + .glyphicon,
    .icon-addon:hover .glyphicon,
    .icon-addon .form-control:focus + .fa,
    .icon-addon:hover .fa {
        color: #2580db;
    }

    #AdropDownButton{
        /*font-size: 2.0em;*/
        /*background-color: red;*/
        border-color:  #ccccb3;
    }

</style>
<div class="row">
    <nav class="navbar navbar-default col-md-12" style="position: fixed; z-index: 5;">
        <div class="row">
            <div class="col-md-1">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <div class="btn-group navbar-btn">
                            <button id ="dropDownButton" data-toggle="dropdown" class="btn btn-danger dropdown-toggle"><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span></button>
                            <ul id = "dropDownMenu" class="dropdown-menu">
                                <li><a href="http://localhost:8080/AkronAutoBodyCMS/">Home</a></li>
                                <li class="divider"></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>

            <!--beginning of search box-->
            <div class="col-md-2" id="search">
                <form:form  class ="form-horizontal" action="${pageContext.request.contextPath}/search" method="post">
                    <div class="form-group">
                        <div class="input-group input-group-md">
                            <div class="icon-addon addon-md">
                                <input type="text" name="searchValue" placeholder="Search" class="form-control" id="add-search-value"/>
                                <label for="search" class="glyphicon glyphicon-search" rel="tooltip" title="email"></label>
                            </div>
                            <span class="input-group-btn">
                                <button id="advancedSearch" class="btn btn-default" type="submit">Go!</button>
                            </span>
                        </div>

                        <!--beginning of advanced search dropdown-->
                        <ul class="nav navbar-nav">
                            <li class="active">
                                <div class="btn-group navbar-btn">
                                    <span class="input-group-btn">
                                        <button id ="AdropDownButton" data-toggle="dropdown" class="btn btn-xs dropdown-toggle">
                                            Advanced
                                            <!--<span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span>-->
                                        </button>

                                        <ul id = "dropDownMenu" class="dropdown-menu">
                                            <li><input id="keywordSearch" type="radio" name="searchType" value="Keyword" checked>Keyword</li>
                                            <li class="divider"></li>
                                            <li><input id="catergorySearch" type="radio" name="searchType" value="Category">Category</li>
                                            <li class="divider"></li>
                                            <li><input id="authorSearch" type="radio" name="searchType" value="Author">Author</li>
                                            <li class="divider"></li>
                                            <li><input id="hashtagSearch" type="radio" name="searchType" value="Hashtag">Hashtag</li>
                                            <!--<li class="divider"></li>-->

                                        </ul>
                                </div>
                            </li>
                        </ul>
                        </span>
                    </div>
                </form:form>

            </div>



            <!--beginning of title box-->
            <div class="col-md-6">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand homeLink" href="http://localhost:8080/AkronAutoBodyCMS/"><h1 style= "position: absolute;width: 100%;left: 0;top: 0;text-align: center;margin: auto;">Akron Auto Body</h1></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <a data-toggle="modal" data-target="#getQuoteModal" style="font-size: x-large; color: #3F8CBF; text-shadow: white 2px 2px 3px; float: right; padding: 7px" >Get Quote</a>
            </div>
        </div>
    </nav>
</div>

<div id="getQuoteModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Get Quote</h4>
            </div>
            <div class="modal-body">

                <form id="quote-form-data" enctype="multipart/form-data">

                    <table class="table table-bordered">
                        <tr>
                            <th>Car Year:</th>
                            <td>
                                <select id = "quote-year" name="year" class="form-control" disabled="true" required="true">
                                    <!--choices loaded through jQuery (quote.js)-->
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>Car Make:</th>
                            <td>
                                <select id = "quote-make" name="make" class="form-control" disabled="true" required="true">
                                    <!--choices loaded through jQuery (quote.js)-->
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>Car Model:</th>
                            <td>
                                <select id = "quote-model" name="model" class="form-control" disabled="true" required="true">
                                    <!--choices loaded through jQuery (quote.js)-->
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>Body Style:</th>
                            <td>
                                <div id="quote-body-style-error" class="errorMsg"></div>
                                <input type="text" id = "quote-body-style" name ="bodyStyle" disabled="true" required="true"/>
                            </td>
                        </tr>
                        <tr>
                            <th>Vin:</th>
                            <td>
                                <div id="quote-vin-error" class="errorMsg"></div>
                                <input type="text" id = "quote-vin" name="vin" disabled="true" required="true"/>
                            </td>
                        </tr>
                        <tr>
                            <th>Description:</th>
                            <td>
                                <div id="quote-description-error" class="errorMsg"></div>
                                <textarea type="text" id = "quote-description" name="description" disabled="true"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <th>File to upload:</th>
                            <td>
                                <div id="quote-upload-error" class="errorMsg"></div>
                                <input id = "quote-upload" disabled="true" type="file" name="userFile"> 
                            </td>
                        </tr>
                        <tr>
                            <th>Email:</th>
                            <td>
                                <div id="quote-email-error" class="errorMsg"></div>
                                <input type="text" id = "quote-email" name="email" disabled="true" required="true"/>
                            </td>
                        </tr>
                        <tr>
                            <th>Phone:</th>
                            <td>
                                <div id="quote-phone-error" class="errorMsg"></div>
                                <input type="text" id = "quote-phone" name="phone" disabled="true" required="true"/>
                            </td>
                        </tr>
                        <tr>
                            <th>Name:</th>
                            <td>
                                <div id="quote-name-error" class="errorMsg"></div>
                                <input type="text" id = "quote-name" name="name" disabled="true" required="true"/>
                            </td>
                        </tr>
                    </table>
                    <div class="row">
                        <div class="col-md-6 text-center">
                            <button id="quote-submit" class ="btn btn-default" value ="submitQuote" disabled="true">Request Quote</button>
                        </div>
                        <div class="col-md-6 text-center">
                            <span id="quote-submission-span" style="font-size: large;"></span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
