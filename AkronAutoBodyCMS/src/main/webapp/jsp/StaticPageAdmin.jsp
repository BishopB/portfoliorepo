<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Admin Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">      
        <style>
            body {
                background-image: url("${pageContext.request.contextPath}/img/bkgdInvert.jpg");
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
                margin: 0 0 100px; /* bottom = footer height */
            }
            html {
                position: relative;
                min-height: 100%;
            }

            .container {
                vertical-align: middle;
                margin: auto;
            }
            .btn-block {
                padding: 3%; 
                border-radius: 10px;
                bottom: 0px;
                position: fixed;
                left: 0;
                bottom: 0;
                height: 100px;
                width: 100%;
            }
            #AdminBlogsTable{
                border-radius:10px; 
                background-color: whitesmoke;
            }
            #pushDown{
                padding-top: 10%;
            }
            #add-form{
                padding-top: 50px;
                display:none;
                background-color: lightcyan;
                border-radius: 5px;
                padding-bottom: 3%;
            }
            #cancel-button{
                position:relative;
                right:40px;
            }
            .errorMsg{
                font-style: italic;
                color: firebrick;
            }
        </style>
    </head>
    <body>

        <jsp:include page="/jsp/stickyHeader.jsp"></jsp:include> 
            <div id="pushDown" class="container">
                <div class="row">
                    <div class="col-md-12">

                        <table id="AdminBlogsTable" class="table">
                            <tr>
                                <th style="border-top: none">Title</th>
                                <th style="border-top: none"></th>
                                <th style="border-top: none"></th>
                            </tr>
                        <c:forEach items="${StaticPageList}" var="staticPage">
                            <tr id="static-page-row-${staticPage.id}">
                                <td><a data-page-id ="${staticPage.id}" id="view-${staticPage.id}" class = "viewBlogLink">${staticPage.title}</a></td>
                                <td><a data-page-id ="${staticPage.id}" class = "editBlogLink">Edit</a></td>
                                <td><a data-page-id="${staticPage.id}" class="deleteBlogLink">Delete</a></td>
                            </tr>
                            <tr style="display:none" id="edit-row-${staticPage.id}" class="editHiddenRow">
                                <td colspan="3"><div id = "editor-cell-${staticPage.id}"></div></td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>
        </div>

        <div id="add-form" class="container">
            <div class ="row">
                <div class ="col-xs-4">
                    <div class="form-group">
                        <label for="add-title" class="col-md-4 control-label">Title:</label>
                        <div class="col-md-8">
                            <div id = "create-entry-error-title" class ="errorMsg"></div>
                            <input type="text" name="add-title" class="form-control" id="add-title" placeholder="Title" />
                        </div>
                    </div>
                </div>
            </div>

            <form id="create-form" class="form-horizontal">
                <div id = "textarea-row" class ="row">
                    <textarea id="mytextarea">Enter New Content Here!</textarea>
                </div>
                <div id = "bottom-buttons" class ="row">
                    <div class="col-md-6">
                        <center><button id = "submitter"  class="btn btn-default pull-right">Create Content</button></center>
                    </div>
                    <div class="col-md-6">
                        <center><button id="cancel-button"  class="btn btn-default" >Cancel</button></center>
                    </div>
                </div>
            </form>
        </div>

        <div id="add-button-holder" class ="container">
            <div class ="row">
                <div class ="col-md-6">
                    <button id = "add-blog-button" class ="btn btn-default btn-lg btn-block">Add</button> 
                </div>
            </div>
        </div>

        <div id="showStaticPageModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body">

                        <div id ="contentView">

                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <div id="errorModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h3 class="modal-title"><span class="glyphicon glyphicon-warning-sign"><strong>Errors</strong></span><span class="glyphicon glyphicon-warning-sign"></span></h3>
                    </div>
                    <div id= "errorBody" class="modal-body">


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        <script type="text/javascript">
            var contextRoot = "${pageContext.request.contextPath}";
        </script>

        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/tinymce/tinymce.min.js"></script>
        <script type="text/javascript">
            tinymce.init({
                selector: "#mytextarea",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste jbimages"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
                relative_urls: false
            });
        </script>
        <script src ="${pageContext.request.contextPath}/js/staticPageAdmin.js"></script>
<script src ='${pageContext.request.contextPath}/js/adminDropdownSelection.js'></script>
<script src="${pageContext.request.contextPath}/js/quote.js"></script>
<script src="${pageContext.request.contextPath}/js/AdvancedSearch.js"></script>

    </body>
</html>