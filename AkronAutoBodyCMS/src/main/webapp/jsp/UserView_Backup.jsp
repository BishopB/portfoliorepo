<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Admin Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">      
        <style>
            body {
            }

            .hidden{
                display:none;
            }

            .unhidden{
                display:block;
            }

            #blogtitle{
                font-size: 2.5em;
            }

            #blogdate{
                font-size: 1em;
            }

            .table {
                border-bottom:0px !important;
            }
            .table th, .table td {
                border: 0px !important;
            }
            .fixed-table-container {
                border:0px !important;
            }

            #blogList{
                padding-top: 7%;

                /*                                right:50%;
                                                left:50%;
                                float: right;*/
            }
            #UserBlogsTable{
                word-wrap: break-word;
            }

            #arrow{
                float:right;
            }
            #stick{
                position: fixed;
                bottom: 0px;
                padding-left: 2%;
            }
            #blog-content{
                word-wrap: break-word;
            }

        </style>
    </head>
    <body class="col-md-12">
        <!--link to top for sticky footer-->
        <a name="top"></a>
        <!--link to top for sticky footer-->

        <jsp:include page="/jsp/stickyHeader.jsp" /> 


            <div id="blogList"class="container">


                <table id="UserBlogsTable" class="table" style="table-layout: fixed; width: 100%;">                 
                    <c:forEach items="${UserBlogsTable}" var="blog">
                        <tr id="blog-row-${blog.id}">
                            <td id="blogtitle" width="90%">${blog.title}</td>
                            <td>${blog.startDate}</td>

                        </tr>
                        <tr>
                            <td colspan="2" id="blog-content" style="word-wrap: break-word;">${blog.content}<hr /></td>
                        </tr>
                    </c:forEach>
                </table>

            </div>







        <!--beginning of fixed footer-->
        <footer class="footer ">

            <div class="row">
                <div class="col-md-2">
                    <h4><a id ="admin-login" href = "${pageContext.request.contextPath}/jsp/BlogAdmin.jsp">Log In</a></h4>
                </div>
                <div class="col-md-2">
                    <h5><a href = "">About Us </a></h5>
                </div>
                <div class="col-md-2">
                    <h5><a href="">Contact Us</a></h5>
                </div>
                <div class="col-md-2">
                    <h5><a href="">Static Page1</a></h5>
                </div>
                <div class="col-md-2">
                    <h5><a href="">Static Page2</a></h5>
                </div>
                <div class="col-md-2">
                    <h5><a href="">Static Page3</a></h5>
                </div>


                <center> 528 S Main St. Akron, OH 44311 | (855) 600-9584</center>

            </div>

        </footer>
        <!--end of fixed footer-->

        <!--beginning of sticky footer: requires link at top under opening body tag-->
        <!--        <footer  class="footer navbar-fixed-bottom">-->

        <div id="stick" class="col-md-push-11 col-md-1">
            <a href="#top"><img src="http://www.free-icons-download.net/images/back-to-top-icon-33007.png"
                                alt="Back to Top" style="width:50px;height:50px;"></a>
        </div>
        <!--</footer>-->
        <!--end of sticky footer-->

        <script type="text/javascript">
            var contextRoot = "${pageContext.request.contextPath}";
        </script>


        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

        <script src="${pageContext.request.contextPath}/js/app.js"></script>

        <script src="${pageContext.request.contextPath}/js/UserView.js"></script>




    </body>
</html>