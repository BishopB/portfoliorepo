<%-- 
    Document   : search
    Created on : Mar 20, 2016, 11:14:59 AM
    Author     : apprentice
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search</title>
        <style>
            #all_text span
            {
                text-decoration:underline;
                background-color:yellow;    
            }
        </style>
    </head>
    <body>



        <h1>Search</h1>
        <div class="row">
            <div class="col-md-5">
                <div class="input-group">
                    <input type="text" id="searchfor"/>
                    <input type="submit" class="btn btn-default pull-right" name="searchType" value="Search"/>

                </div>
            </div>

            <br>
            <div class ="col-md-5">
                <form:form class ="form-horizontal" action="${pageContext.request.contextPath}/search" method="post">
                    <div class="form-group">
                        <label for="add-search-value" class="col-md-4 control-label">Advanced Search</label>
                        <div class="col-md-8">
                            <input type="text" name="searchValue" class="form-control" id="add-search-value" placeholder="Search" />
                        </div>
                    </div>

                    <input type="submit" class="btn btn-default pull-right" name="searchType" value="Hashtag"/>
                    <input type="submit" class="btn btn-default pull-right" name="searchType" value="Category"/>
                    <input type="submit" class="btn btn-default pull-right" name="searchType" value="Author"/>
                    <input type="submit" class="btn btn-default pull-right" name="searchType" value="Keyword"/>
                </form:form>
            </div>


        </div>
        <div class="row">
            <div class="col-md-10">
                <table>
                    <tr>


                    </tr>
                    <c:forEach items="${results}" var="result">
                        <tr>
                            <td>${result.title}</td>
                            <td> ${result.startDate}</td>
                        </tr>



                        <tr>
                            <td colspan="2" id="blog-content" style="word-wrap: break-word;">${result.content}<hr /></td>
                        </tr>
                    </c:forEach>
                </table>
            </div>

        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.0.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/basicSearch.js"></script>
        <script src="${pageContext.request.contextPath}/js/UserView.js"></script>
        <script src ='${pageContext.request.contextPath}/js/adminDropdownSelection.js'></script>
        <script src="${pageContext.request.contextPath}/js/quote.js"></script>
        <script src="${pageContext.request.contextPath}/js/AdvancedSearch.js"></script>

    </body>
</html>
