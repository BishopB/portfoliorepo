<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Admin Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">      
        <style>
            body {
                background-image: url("${pageContext.request.contextPath}/img/bkgdInvert.jpg");
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
                margin: 0 0 100px; /* bottom = footer height */
            }
            html {
                position: relative;
                min-height: 100%;
            }

            .container {
                vertical-align: middle;
                margin: auto;
            }
            .btn-block {
                padding: 3%; 
                border-radius: 10px;
                bottom: 0px;
                position: fixed;
                left: 0;
                bottom: 0;
                height: 100px;
                width: 100%;
            }
            .errorMsg{
                font-style: italic;
                color: firebrick;
            }
            #add-form{
                background-color: lightcyan;
                border-radius: 5px;
                padding-bottom: 3%;
                display: none;
            }
            .glyphicon-ok{
                color:green;
            }
            .glyphicon-exclamation-sign{
                color: gold;
            }
            #pushDown{
                padding-top: 10%;
            }
        </style>
    </head>
    <jsp:include page="/jsp/stickyHeader.jsp"></jsp:include>       


        <div class ="container" id="pushDown">
            <div class="row">
                <div class="col-md-12">

                    <table id="AdminBlogsTable" class="table" style = "border-radius:10px; background-color: whitesmoke;">
                        <tr>
                            <th style="border-top: none">Title</th>
                            <th style="border-top: none"></th>
                            <th style="border-top: none"></th>
                        </tr>
                    <c:forEach items="${blogsList}" var="blog">
                        <tr id="blogs-row-${blog.id}">
                            <td><a data-blog-id ="${blog.id}" id="view-${blog.id}" class = "viewBlogLink">${blog.title}</a></td>
                            <td><a data-blog-id ="${blog.id}" class = "editBlogLink">Edit</a></td>
                            <td><a data-blog-id="${blog.id}" class="deleteBlogLink">Delete</a></td>
                        </tr>
                        <tr style="display:none" id="edit-row-${blog.id}" class="editHiddenRow">
                            <td colspan="3"><div id = "editor-cell-${blog.id}"></div></td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
    </div>

    <div id="add-form" class="container" style="padding-top: 50px;">
        <div class="col-md-10 col-md-push-1">
            <div class="row" style="padding-top: 15px">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="add-category" class="col-md-5 control-label">Category:</label>
                        <div class="col-md-7">
                            <select id="add-category">
                                <option value="">Category </option>
                                <c:forEach items="${categoryList}" var="category">
                                    <option value="${category.id}">${category.category}</option> 
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="add-start-date" class="col-md-5 control-label">Start Date:</label>
                        <div class="col-md-7">
                            <input type="date" name="start-date" class="form-control" id="add-start-date" placeholder="start date" />
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="add-title" class="col-md-5 control-label">Title:</label>
                        <div class="col-md-7">
                            <input type="text" name="title" class="form-control" id="add-title" placeholder="title" />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="add-end-date" class="col-md-5 control-label">End Date:</label>
                        <div class="col-md-7">
                            <input type="date" name="end-date" class="form-control" id="add-end-date" placeholder="end date" />
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <form id="create-form" class="form-horizontal">
                <div id = "textarea-row" class="row">
                    <textarea id="mytextarea">Enter New Content Here!</textarea>
                </div>
                <div id = "bottom-buttons" class ="row">
                    <div class="col-md-3">
                        <center><button id = "submitter"  class="btn btn-default pull-right">Submit</button></center>
                    </div>
                    <div class="col-md-3">
                        <center><input type="button" class="btn btn-default" value="Cancel" id="cancel-button" /></center>
                    </div>
                    <div class="col-md-6">
                        <div id="status-button-holder" class ="pull-right" >
                        </div>
                    </div>
                </div>
                <input type="hidden" id="primaryKey" />
            </form>


        </div>
    </div>

    <div id="add-button-holder" class ="container">
        <div class ="row">
            <div class ="col-md-6">
                <button id = "add-blog-button" class ="btn btn-default btn-lg btn-block">Add</button> 
            </div>
        </div>
    </div>

    <div id="showBlogModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">

                    <div id ="contentView">

                    </div>


                </div>
                <div id = "showFooter" class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>




    <div id="statusBlogModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">


                    <table class="table table-bordered">

                        <tr>
                            <td><label class="radio inline">
                                    <div class="col-md-6 text-center">
                                        <input type="radio" name="status" class="statusClass" value="Approve" disabled/>Approve
                                    </div>

                                    <div class="col-md-6 text-center">
                                        <input type="radio" name="status" class="statusClass" value="Pending" disabled/> Pending
                                    </div>

                                </label></td>
                        </tr>

                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>




    <div id="errorModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title"><span class="glyphicon glyphicon-warning-sign"><strong>Errors</strong></span><span class="glyphicon glyphicon-warning-sign"></span></h3>
                </div>
                <div id= "errorBody" class="modal-body">


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript">
        var contextRoot = "${pageContext.request.contextPath}";

    </script>


    <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/blogAdmin.js"></script>
    <script src ='${pageContext.request.contextPath}/js/adminDropdownSelection.js'></script>
    <script src="${pageContext.request.contextPath}/js/tinymce/tinymce.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/quote.js"></script>
        <script src="${pageContext.request.contextPath}/js/AdvancedSearch.js"></script>
    <script>
//        tinymce.init({
//            selector: '#mytextarea',
//            plugins: ["image"],
//            image_list: [
//                {title: 'My image 1', value: '$ {pageContext.request.contextPath}/uploadFile/showImage/32'},
//                {title: 'My image 2', value: 'http://www.moxiecode.com/my2.gif'}
//            ]
//        });
        tinymce.init({
                selector: "#mytextarea",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste jbimages"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
                relative_urls: false
            });
    </script>
</body>

</html>