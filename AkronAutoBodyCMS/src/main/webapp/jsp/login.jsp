<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Admin Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">      
        <style>
            body {
                background-image: url("${pageContext.request.contextPath}/img/bkgdInvert.jpg");
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
                margin: 0 0 100px; /* bottom = footer height */
            }
            html {
                position: relative;
                min-height: 100%;
            }

            .container {
                vertical-align: middle;
                margin: auto;
            }
            .btn-block {
                padding: 3%; 
                border-radius: 10px;
                bottom: 0px;
                position: fixed;
                left: 0;
                bottom: 0;
                height: 100px;
                width: 100%;
            }
            .dropdown-offset + a {
                width: 6%;
                margin-left: 3%;
            }
            .dropdown-offset {
                width: 90%;
                display: inline-block;
            }
            #pushDown{
                padding-top: 10%;
            }
            .table-striped > tbody > tr:nth-child(odd) > td, .table-striped > tbody > tr:nth-child(odd) > th {
                background-color: threedface;
            }
            .table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
                background-color: lightblue;
            }
        </style>
    </head>
    <jsp:include page="/jsp/stickyHeader.jsp"></jsp:include>       


        <div id="pushDown" class ="container" >
            <div class="row">
                <div class="col-md-12">

                    <form method="POST" action="login/authenticate">
                        <table class="table table-boredered table-striped">
                            <tr>
                                <td>
                                    Username: <input type="text" name="username" class="form-control" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Password: <input type="password" name="password" class="form-control" />
                                </td>
                            </tr>
                        </table>
                        <div class="text-center">
                            <input type="submit" value="Sign in" class="btn btn-default"/>
                        </div>
                    </form>
                <c:if test="${param.login_error == 1}">
                    <h3>Wrong username or password</h3>
                </c:if>
            </div>
        </div>
    </div>

</body>

<script type="text/javascript">
    var contextRoot = "${pageContext.request.contextPath}";

</script>


<script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script src ='${pageContext.request.contextPath}/js/adminDropdownSelection.js'></script>
<script src="${pageContext.request.contextPath}/js/quote.js"></script>
<script src="${pageContext.request.contextPath}/js/AdvancedSearch.js"></script>


<!--<script src="${pageContext.request.contextPath}/js/Quote.js"></script>-->
</body>

</html>