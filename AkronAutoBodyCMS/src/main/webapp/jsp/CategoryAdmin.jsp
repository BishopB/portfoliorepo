<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Admin Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">      
        <style>
            body {
                background-image: url("${pageContext.request.contextPath}/img/bkgdInvert.jpg");
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
                margin: 0 0 100px; /* bottom = footer height */
            }
            html {
                position: relative;
                min-height: 100%;
            }

            .container {
                vertical-align: middle;
                margin: auto;
            }
            .btn-block {
                padding: 3%; 
                border-radius: 10px;
                bottom: 0px;
                position: fixed;
                left: 0;
                bottom: 0;
                height: 100px;
                width: 100%;
            }
            #pushDown{
                padding-top: 10%;
            }
        </style>
    </head>
    <body>
    <jsp:include page="/jsp/stickyHeader.jsp"></jsp:include>       
    

        <div class ="container" id="pushDown">
            <div class="row">
                <div class="col-md-12">

                    <table id="CategoryTable" class="table" style = "border-radius:10px; background-color: whitesmoke;">
                        <tr>
                            <th style="border-top: none">Category Name:</th>
                            <th style="border-top: none"></th>
                            <th style="border-top: none"></th>
                        </tr>
                    <c:forEach items="${categorysList}" var="category">
                        <tr id="categorys-row-${category.id}">
                            <td><a data-category-id ="${category.id}" class = "viewCategoryLink" data-toggle="modal" data-target="#showCategoryModal">${category.category}</a></td>
                            <td><a data-category-id ="${category.id}" class = "editCategoryLink" data-toggle="modal" data-target="#editCategoryModal">Edit</a></td>
                            <td><a data-category-id="${category.id}" class="delete-link">Delete</a></td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
    </div>

    <div id="addCategoryModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Category</h4>
                </div>
                <div class="modal-body">

                    <form id="create-form">
                        <table class="table table-bordered">

                            <tr>
                                <th>Name</th>
                                <td>
                                    <input type="text" id="add-name" class="form-control"/>
                                </td>
                            </tr>

                        </table>

                        <div id="add-category-validation-errors"></div>
                        <input type="hidden" id="add-id" />
                        <input type="submit"  id="add-category-button" class="btn btn-default" />
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>      


    <div id="showCategoryModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Category Details</h4>
                </div>
                <div class="modal-body">


                    <table class="table table-bordered">

                        <tr>
                            <th>Name:</th>
                            <td id="category-name"></td>
                        </tr>
                        
                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>  

    <div id="editCategoryModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Category</h4>
                </div>
                <div class="modal-body">


                    <table class="table table-bordered">

                        <tr>
                            <th>Name</th>
                            <td>
                                <input type="text" id="edit-name" class="form-control"/>
                            </td>
                        </tr>

                    </table>

                    <input type="hidden" id="edit-id" />
                    <button  id="edit-category-button" class="btn btn-default" >Edit Category</button>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>      


    <!--    <input type="button" onclick="unhide(this, 'add')" value="add" id="hiddenButton" >-->
    <div id="add-button-holder" class ="container">
        <div class ="row">
            <div class ="col-md-6">
                <button id = "add-blog-button" class ="btn btn-default btn-lg btn-block" data-toggle="modal" data-target="#addCategoryModal">Add</button> 
            </div>
        </div>
    </div>
</body>


<script type="text/javascript">
    var contextRoot = "${pageContext.request.contextPath}";

</script>


<script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/Category.js"></script>
<script src ='${pageContext.request.contextPath}/js/adminDropdownSelection.js'></script>
<script src="${pageContext.request.contextPath}/js/quote.js"></script>
<script src="${pageContext.request.contextPath}/js/AdvancedSearch.js"></script>

</body>

</html>