<%-- 
Used to communicate with the plugin to relay the file and the success/fail of the upload. 
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Upload Result Container for JBImages Plugin</title>
    </head>
    <body>
        <script language="javascript" type="text/javascript">
            window.parent.window.jbImagesDialog.uploadFinish({
                filename: '${file_name}',
                result: '${result}',
                resultCode: '${resultcode}'
            });
        </script>
    </body>
</html>
