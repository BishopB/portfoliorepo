<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Admin Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">      
        <style>
            body {
                background-image: url("${pageContext.request.contextPath}/img/bkgdInvert.jpg");
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
                margin: 0 0 100px; /* bottom = footer height */
            }
            html {
                position: relative;
                min-height: 100%;
            }

            .container {
                vertical-align: middle;
                margin: auto;
            }
            .btn-block {
                padding: 3%; 
                border-radius: 10px;
                bottom: 0px;
                position: fixed;
                left: 0;
                bottom: 0;
                height: 100px;
                width: 100%;
            }
            #pushDown{
                padding-top: 10%;
            }
        </style>
    </head>
    <jsp:include page="/jsp/stickyHeader.jsp"></jsp:include>       


        <div class ="container" id="pushDown">
            <div class="row">
                <div class="col-md-12">

                    <table id="AccountTable" class="table" style = "border-radius:10px; background-color: whitesmoke;">
                        <tr>
                            <th style="border-top: none">Account Name:</th>
                            <th style="border-top: none"></th>
                            <th style="border-top: none"></th>
                        </tr>
                    <c:forEach items="${accountsList}" var="account">
                        <tr id="accounts-row-${account.id}">
                            <td><a data-account-id ="${account.id}" class = "viewAccountLink" data-toggle="modal" data-target="#showAccountModal">${account.name}</a></td>
                            <td><a data-account-id ="${account.id}" class = "editAccountLink" data-toggle="modal" data-target="#editAccountModal">Edit</a></td>
                            <td><a data-account-id="${account.id}" class="delete-link">Delete</a></td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
    </div>


    <div id="addAccountModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Account</h4>
                </div>
                <div class="modal-body">

                    <form id="create-form">
                        <table class="table table-bordered">

                            <tr>
                                <th>Name</th>
                                <td>
                                    <input type="text" id="add-name" class="form-control"/>
                                </td>
                            </tr>
                            <tr>
                                <th>UserName</th>
                                <td><input type="text" id="add-email" class="form-control"/></td>
                            </tr>             
                            <tr>
                                <th>New Password:</th>
                                <td><input type="text" id="create-password" class="form-control"/></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <label class="radio inline" >
                                        <span style="margin-left:30px"><input type="radio" name="optradio" class="admin" value="ROLE_ADMIN" id="create-account-admin"checked/>Admin</span>
                                        <span style="margin-left:40px"><input type="radio" name="optradio" class="admin" value="ROLE_SUB_ADMIN"/>Sub-Admin </span>
                                    </label>                                  
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <label class="checkbox inline">
                                        <span style="margin-left:30px">
                                            <input type="checkbox" id="enabled" value="enabled"/>enabled
                                        </span>
                                    </label>
                                </td>
                            </tr>

                        </table>

                        <div id="add-account-validation-errors"></div>
                        <input type="hidden" id="add-id" />
                        <input type="submit"  id="add-account-button" class="btn btn-default" />
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>      


    <div id="showAccountModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Account Details</h4>
                </div>
                <div class="modal-body">


                    <table class="table table-bordered">

                        <tr>
                            <th>Name:</th>
                            <td id="account-name"></td>
                        </tr>
                        <tr>
                            <th>Username:</th>
                            <td id="account-email"></td>
                        </tr>
                        <tr>
                            <th>Authorization level:</th>
                            <td id="account-authorization"></td>
                        </tr>   
                        <tr>
                            <th>Enabled/Disabled:</th>
                            <td id="account-enabled"></td>
                        </tr>   
                    </table>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>  








    <div id="editAccountModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Account</h4>
                </div>
                <div class="modal-body">

                    <form id="edit-account-from">
                        <table class="table table-bordered">

                            <tr>
                                <th>Name</th>
                                <td>
                                    <input type="text" id="edit-name" class="form-control"/>
                                </td>
                            </tr>
                            <tr>
                                <th>Username</th>
                                <td><input type="text" id="edit-email" class="form-control"/></td>
                            </tr>             
                            <tr>
                                <th>New Password:</th>
                                <td><input type="text" id="edit-password" class="form-control"/></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <label class="radio inline" >
                                        <span style="margin-left:30px"><input type="radio" name="optradio" class="edit-admin" value="ROLE_ADMIN"/>Admin</span>
                                        <span style="margin-left:40px"><input type="radio" name="optradio" class="edit-admin" value="ROLE_SUB_ADMIN"/>Sub-Admin </span>
                                    </label>                                  
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <label class="checkbox inline">
                                        <span style="margin-left:30px">
                                            <input type="checkbox" class="edit-enabled" value="enabled"/>enabled
                                            <input type="hidden" class="edit-enabled" value="disabled" />
                                        </span>
                                    </label>
                                </td>
                            </tr>



                        </table>

                        <div id="edit-account-validation-errors"></div>
                        <input type="hidden" id="edit-id" />
                        <button  id="edit-account-button" class="btn btn-default" >Edit Account</button>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>      


    <!--    <input type="button" onclick="unhide(this, 'add')" value="add" id="hiddenButton" >-->
    <div id="add-button-holder" class ="container">
        <div class ="row">
            <div class ="col-md-6">
                <button id = "add-blog-button" class ="btn btn-default btn-lg btn-block" data-toggle="modal" data-target="#addAccountModal">Add</button> 
            </div>
        </div>
    </div>
</body>


<script type="text/javascript">
    var contextRoot = "${pageContext.request.contextPath}";

</script>


<script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/Account.js"></script>
<script src ='${pageContext.request.contextPath}/js/adminDropdownSelection.js'></script>
<script src="${pageContext.request.contextPath}/js/tinymce/tinymce.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/quote.js"></script>
        <script src="${pageContext.request.contextPath}/js/AdvancedSearch.js"></script>
<script>
    tinymce.init({
        selector: '#mytextarea',
        plugins: ["image"],
        image_list: [
            {title: 'My image 1', value: '${pageContext.request.contextPath}/uploadFile/showImage/32'},
            {title: 'My image 2', value: 'http://www.moxiecode.com/my2.gif'}
        ]
    });
</script>
</body>

</html>