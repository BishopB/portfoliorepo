<%-- 
    Document   : stickyHeader
    Created on : Mar 10, 2016, 2:14:41 PM
    Author     : apprentice
--%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>



<style>
    
#footer {
  position: absolute;
  bottom: 0;
  width: 100%;
  /* Set the fixed height of the footer here */
  height: 60px;
  background-color: #f5f5f5;
}
</style>

<div id="footer">
      <div class="container">
        <p class="muted credit">This is a sticky footer</p>
      </div>
</div>