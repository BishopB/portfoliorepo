<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Admin Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">      
        <style>
            body {
                background-image: url("${pageContext.request.contextPath}/img/bkgdInvert.jpg");
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
                margin: 0 0 100px; /* bottom = footer height */
            }
            html {
                position: relative;
                min-height: 100%;
            }

            .container {
                vertical-align: middle;
                margin: auto;
            }
            .btn-block {
                padding: 3%; 
                border-radius: 10px;
                bottom: 0px;
                position: fixed;
                left: 0;
                bottom: 0;
                height: 100px;
                width: 100%;
            }
            .dropdown-offset + a {
                width: 6%;
                margin-left: 3%;
            }
            .dropdown-offset {
                width: 90%;
                display: inline-block;
            }
            #pushDown{
                padding-top: 10%;
            }
        </style>
    </head>
    <jsp:include page="/jsp/stickyHeader.jsp"></jsp:include>       


        <div class ="container" id="pushDown">
            <div class="row">
                <div class="col-md-12">

                    <table id="QuoteTable" class="table" style = "border-radius:10px; background-color: whitesmoke;">
                        <tr>
                            <th style="border-top: none">Quote Email:</th>
                            <th style="border-top: none"></th>
                            <th style="border-top: none"></th>
                        </tr>
                    <c:forEach items="${quoteList}" var="quote">
                        <tr id="quote-row-${quote.id}">
                            <td><a data-quote-id ="${quote.id}" class = "viewQuoteLink" data-toggle="modal" data-target="#showQuoteModal">${quote.email}</a></td>
                            <td><a data-quote-id ="${quote.id}" class = "editQuoteLink" data-toggle="modal" data-target="#editQuoteModal">Edit</a></td>
                            <td><a data-quote-id="${quote.id}" class="delete-link">Delete</a></td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
    </div>

    <div id="addQuoteModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Quote</h4>
                </div>
                <div class="modal-body">

                    <form id="create-form">
                        <table class="table table-bordered">

                            <tr>
                                <th>Name</th>
                                <td>
                                    <input type="text" id="add-name" class="form-control"/>
                                </td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td><input type="text" id="add-email" class="form-control"/></td>
                            </tr>             
                            <tr>
                                <th>New Password:</th>
                                <td><input type="text" id="create-password" class="form-control"/></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <label class="radio inline" >
                                        <span style="margin-left:30px"><input type="radio" name="optradio" id="admin" value="ROLE_ADMIN" checked="checked"/>Admin</span>
                                        <span style="margin-left:40px"><input type="radio" name="optradio" id="admin" value="ROLE_SUB_ADMIN"/>Sub-Admin </span>
                                    </label>                                  
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <label class="checkbox inline">
                                        <span style="margin-left:30px">
                                            <input type="checkbox" value="enabled" id="enabled" value="true"/>enabled
                                        </span>
                                    </label>
                                </td>
                            </tr>

                        </table>

                        <div id="add-quote-validation-errors"></div>
                        <input type="hidden" id="add-id" />
                        <input type="submit"  id="add-quote-button" class="btn btn-default" />
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>      


    <div id="showQuoteModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Quote Details</h4>
                </div>
                <div class="modal-body">

                    <table class="table table-bordered">
                        <tr>
                            <th>Car Year:</th>
                            <td>
                                <input type="text" id = "show-quote-year" name="year" disabled="true"/>
                            </td>
                        </tr>
                        <tr>
                            <th>Car Make:</th>
                            <td>
                                <input type="text" id = "show-quote-make" name="make" disabled="true"/>
                            </td>
                        </tr>
                        <tr>
                            <th>Car Model:</th>
                            <td>
                                <input type="text" id = "show-quote-model" name="model" disabled="true"/>
                            </td>
                        </tr>
                        <tr>
                            <th>Body Style:</th>
                            <td>
                                <div id="quote-body-style-error" class="errorMsg"></div>
                                <input type="text" id = "show-quote-body-style" name ="bodyStyle" disabled="true"/>
                            </td>
                        </tr>
                        <tr>
                            <th>Vin:</th>
                            <td>
                                <div id="quote-vin-error" class="errorMsg"></div>
                                <input type="text" id = "show-quote-vin" name="vin" disabled="true"/>
                            </td>
                        </tr>
                        <tr>
                            <th>Description:</th>
                            <td>
                                <div id="quote-description-error" class="errorMsg"></div>
                                <textarea type="text" id = "show-quote-description" name="description" disabled="true"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <th>Attached File:</th>
                            <td>
                                <span id="show-quote-file"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>Email:</th>
                            <td>
                                <div id="quote-email-error" class="errorMsg"></div>
                                <input type="text" id = "show-quote-email" name="email" disabled="true"/>
                            </td>
                        </tr>
                        <tr>
                            <th>Phone:</th>
                            <td>
                                <div id="quote-phone-error" class="errorMsg"></div>
                                <input type="text" id = "show-quote-phone" name="phone" disabled="true"/>
                            </td>
                        </tr>
                        <tr>
                            <th>Name:</th>
                            <td>
                                <div id="quote-name-error" class="errorMsg"></div>
                                <input type="text" id = "show-quote-name" name="name" disabled="true"/>
                            </td>
                        </tr>
                    </table>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>  








    <div id="editQuoteModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Quote</h4>
                </div>
                <div class="modal-body">

                    <form id="edit-quote-form-data" enctype="multipart/form-data">
                        <input type="hidden" id = "edit-quote-id" name="id" value=""/>
                        <input type="hidden" id = "edit-quote-downloadLink-hidden" name="downloadLink" value=""/>

                        <table class="table table-bordered">
                            <tr>
                                <th>Car Year:</th>
                                <td>
                                    <select id = "edit-quote-year" name="year" class="form-control dropdown-offset" disabled="true" required="true">
                                        <!--choices loaded through jQuery (quote.js)-->
                                    </select>
                                    <a id="edit-quote-year-reset"><span class="glyphicon glyphicon-repeat"></span></a>
                                </td>
                            </tr>
                            <tr>
                                <th>Car Make:</th>
                                <td>
                                    <select id = "edit-quote-make" name="make" class="form-control dropdown-offset" disabled="true" required="true">
                                        <!--choices loaded through jQuery (quote.js)-->
                                    </select>
                                    <a id="edit-quote-make-reset"><span class="glyphicon glyphicon-repeat"></span></a>
                                </td>
                            </tr>
                            <tr>
                                <th>Car Model:</th>
                                <td>
                                    <select id = "edit-quote-model" name="model" class="form-control dropdown-offset" disabled="true" required="true">
                                        <!--choices loaded through jQuery (quote.js)-->
                                    </select>
                                    <a id="edit-quote-model-reset"><span class="glyphicon glyphicon-repeat"></span></a>
                                </td>
                            </tr>
                            <tr>
                                <th>Body Style:</th>
                                <td>
                                    <div id="edit-quote-body-style-error" class="errorMsg"></div>
                                    <input type="text" id = "edit-quote-body-style" name ="bodyStyle" required="true"/>
                                </td>
                            </tr>
                            <tr>
                                <th>Vin:</th>
                                <td>
                                    <div id="edit-quote-vin-error" class="errorMsg"></div>
                                    <input type="text" id = "edit-quote-vin" name="vin" required="true"/>
                                </td>
                            </tr>
                            <tr>
                                <th>Description:</th>
                                <td>
                                    <div id="edit-quote-description-error" class="errorMsg"></div>
                                    <textarea type="text" id = "edit-quote-description" name="description" ></textarea>
                                </td>
                            </tr>
                            <tr>
                                <th>Uploaded File:</th>
                                <td>
                                    <div id="edit-quote-upload-error" class="errorMsg"></div>
                                    <div class="row">
                                        <div id = "edit-quote-file-div" class="col-md-5">
                                        <input id = "edit-quote-file" type="file" name="userFile">
                                        </div>
                                    </div> 
                                </td>
                            </tr>
                            <tr>
                                <th>Email:</th>
                                <td>
                                    <div id="edit-quote-email-error" class="errorMsg"></div>
                                    <input type="text" id = "edit-quote-email" name="email" required="true"/>
                                </td>
                            </tr>
                            <tr>
                                <th>Phone:</th>
                                <td>
                                    <div id="edit-quote-phone-error" class="errorMsg"></div>
                                    <input type="text" id = "edit-quote-phone" name="phone" required="true"/>
                                </td>
                            </tr>
                            <tr>
                                <th>Name:</th>
                                <td>
                                    <div id="edit-quote-name-error" class="errorMsg"></div>
                                    <input type="text" id = "edit-quote-name" name="name" required="true"/>
                                </td>
                            </tr>
                        </table>
                        <div class="row">
                            <div class="col-md-6 text-center">
                                <button id="edit-quote-submit" class ="btn btn-default" value ="submitQuote" >Revise Quote</button>
                            </div>
                            <div class="col-md-6 text-center">
                                <span id="edit-quote-submission-span" style="font-size: large;"></span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>    


    <!--    <input type="button" onclick="unhide(this, 'add')" value="add" id="hiddenButton" >-->
    <div id="add-button-holder" class ="container">
        <div class ="row">
            <div class ="col-md-6">
                <button id = "add-blog-button" class ="btn btn-default btn-lg btn-block" data-toggle="modal" data-target="#getQuoteModal">Add</button> 
            </div>
        </div>
    </div>
</body>


<script type="text/javascript">
    var contextRoot = "${pageContext.request.contextPath}";

</script>


<script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script src ='${pageContext.request.contextPath}/js/adminDropdownSelection.js'></script>
<script src="${pageContext.request.contextPath}/js/quote.js"></script>
<script src="${pageContext.request.contextPath}/js/AdvancedSearch.js"></script>


<!--<script src="${pageContext.request.contextPath}/js/Quote.js"></script>-->
</body>

</html>