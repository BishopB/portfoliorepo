/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DaoTests;

import com.thesoftwareguild.akronautobodycms.dao.AccountInf;
import com.thesoftwareguild.akronautobodycms.dto.Account;
import com.thesoftwareguild.akronautobodycms.dto.Blog;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class AccountDaoTests {

    private ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
    private AccountInf accountDao = ctx.getBean("accountDao", AccountInf.class);
    private JdbcTemplate template = ctx.getBean("testJdbcTemplate", JdbcTemplate.class);
    private Account target1 = new Account();
    private Account target2 = new Account();
    private Account target3 = new Account();
    private Account expected1;
    private Account expected2;
    private Account expected3;
    private Integer id1, id2, id3;

    public AccountDaoTests() {

    }

    @Before
    public void setUp() {
        template.update("DELETE FROM `authorities` WHERE id >3");
        template.update("DELETE FROM `account` WHERE account_id >3");
        target1.setEmail("matt@yahoo.com");
        target1.setName("Matt");
        target1.setPassword("password");
        target1.setEnabled(true);
        target1.getAuthorities().add("ROLE_USER");
        target1.getAuthorities().add("ROLE_ADMIN");
        expected1 = accountDao.create(target1);
        id1 = expected1.getId();

        target2.setEmail("Brian@gmail.com");
        target2.setName("Brian");
        target2.setPassword("passwud");
        target2.setEnabled(true);
        target2.getAuthorities().add("ROLE_USER");
        target2.getAuthorities().add("ROLE_ADMIN");
        expected2 = accountDao.create(target2);
        id2 = expected2.getId();

        target3.setEmail("itsqavi@hotmail.com");
        target3.setName("Qavi");
        target3.setPassword("wordpass");
        target3.setEnabled(false);
        target3.getAuthorities().add("ROLE_USER");
        expected3 = accountDao.create(target3);
        id3 = expected3.getId();

    }

    @After
    public void tearDown() {

        template.update("DELETE FROM `authorities` WHERE id >3");
        template.update("DELETE FROM `account` WHERE account_id >3");
    }

    @Test
    public void createAccountTest() {
        Assert.assertEquals(target1, expected1);
        Assert.assertEquals(target2, expected2);
        Assert.assertEquals(target3, expected3);
        Assert.assertTrue(!expected1.getId().toString().isEmpty());
        Assert.assertTrue(!expected2.getId().toString().isEmpty());
        Assert.assertTrue(!expected3.getId().toString().isEmpty());
    }

    @Test
    public void getAccountTest() {
        int expectedId = expected1.getId();
        Account expectedAccount1 = accountDao.read(expectedId);
        Account expectedAccount2 = accountDao.read(id2);
        Account expectedAccount3 = accountDao.read(id3);
        Assert.assertEquals(expected1.getId(), expectedAccount1.getId());
        Assert.assertTrue(expectedAccount1.getAuthorities().contains("ROLE_ADMIN"));
        Assert.assertTrue(expectedAccount1.getAuthorities().contains("ROLE_USER"));
        Assert.assertTrue(expectedAccount2.getAuthorities().contains("ROLE_ADMIN"));
        Assert.assertTrue(expectedAccount2.getAuthorities().contains("ROLE_USER"));
        Assert.assertTrue(expectedAccount3.getAuthorities().contains("ROLE_USER"));

    }

    @Test
    public void updateAccountTest() {
        target1.setName("Wizard of the West");
        expected1 = accountDao.update(target1);
        Assert.assertEquals(target1.getName(), expected1.getName());

        target2.setName("Wizard of the East");
        expected2 = accountDao.update(target2);
        Assert.assertEquals(target2.getName(), expected2.getName());

        target3.setName("Wizard of the South");
        expected3 = accountDao.update(target3);
        Assert.assertEquals(target3.getName(), expected3.getName());
    }

}
