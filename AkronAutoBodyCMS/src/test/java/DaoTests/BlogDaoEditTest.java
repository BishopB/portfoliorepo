/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DaoTests;

import com.thesoftwareguild.akronautobodycms.dao.BlogDaoInf;
import com.thesoftwareguild.akronautobodycms.dto.Account;
import com.thesoftwareguild.akronautobodycms.dto.Blog;
import com.thesoftwareguild.akronautobodycms.dto.Category;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class BlogDaoEditTest {

    ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");

    BlogDaoInf blogDao = ctx.getBean("blogDao", BlogDaoInf.class);
    JdbcTemplate template = ctx.getBean("testJdbcTemplate", JdbcTemplate.class);
    Blog target = new Blog();
    Blog target2 = new Blog();
    Blog target3 = new Blog();

    Integer expected1Id = -1;
    Integer expected2Id = -2;
    Integer expected3Id = -3;

    Blog expected1;
    Blog expected2;
    Blog expected3;
    List<Blog> testList = new ArrayList();

    @Before
    public void setUp() {

        LocalDate date = LocalDate.parse("2020-03-15");
        LocalDate dateEnd = LocalDate.parse("2020-07-11");
        target.setStartDate(date);
        target.setEndDate(date);
        List<String> hashtagList = new ArrayList();
        String hashtag = "hash1";
        hashtagList.add(hashtag);
        target.setHashtag(hashtagList);
        target.setTitle("newblogTest1");

        Account account = new Account();
        account.setId(1);
        target.setAccount(account);
        target.setContent("This is my first# blog post EVER!");
        target.setAnchor("an Anchor1");
        Category mockCategory = new Category();
        mockCategory.setId(1);
        target.setCategory(mockCategory);
        target.setApproved(true);
        expected1 = blogDao.create(target);
        expected1Id = expected1.getId();

        testList.add(target);

        LocalDate date2 = LocalDate.parse("2019-04-09");
        LocalDate dateEnd2 = LocalDate.parse("2019-06-11");
        target2.setStartDate(date2);
        target2.setEndDate(dateEnd2);
        List<String> hashtagList2 = new ArrayList();
        String hashtag2 = "hash2";
        hashtagList2.add(hashtag2);
        target2.setHashtag(hashtagList2);
        target2.setTitle("newblogTest2");

        Account account2 = new Account();
        account.setId(2);
        target2.setAccount(account);
        target2.setContent("This is my second b#log post EVER!");
        target2.setAnchor("an Anchor2");
        Category mockCategory2 = new Category();
        mockCategory2.setId(2);
        target2.setCategory(mockCategory2);
        target2.setApproved(true);
        expected2 = blogDao.create(target2);
        expected2Id = template.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        testList.add(target2);

        LocalDate date3 = LocalDate.parse("2018-04-09");
        LocalDate dateEnd3 = LocalDate.parse("2018-07-11");
        target3.setStartDate(date3);
        target3.setEndDate(dateEnd3);
        List<String> hashtagList3 = new ArrayList();
        String hashtag3 = "hash3";
        hashtagList3.add(hashtag3);
        target3.setHashtag(hashtagList3);
        target3.setTitle("newblogTest3");

        Account account3 = new Account();
        account.setId(3);
        target3.setAccount(account);
        target3.setContent("This is my third blog post EVER!");
        target3.setAnchor("an Anchor3");
        Category mockCategory3 = new Category();
        mockCategory3.setId(3);
        target3.setCategory(mockCategory3);
        target3.setApproved(false);
        expected3 = blogDao.create(target3);
        expected3Id = template.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

        testList.add(target3);

    }

    @After
    public void tearDown() {
        template.update("DELETE FROM blog WHERE blog_id=?", expected1Id);
        template.update("DELETE FROM blog WHERE blog_id=?", expected2Id);
        template.update("DELETE FROM blog WHERE blog_id=?", expected3Id);
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void editOneBlog() {
        LocalDate date = null;
        LocalDate dateEnd = null;
        date = LocalDate.parse("2022-04-05");
        dateEnd = LocalDate.parse("2023-04-11");
        expected1.setStartDate(date);
        expected1.setEndDate(dateEnd);
        List<String> hashtagList = new ArrayList();
        String hashtag = "#has221";
        hashtagList.add(hashtag);
        expected1.setHashtag(hashtagList);
        expected1.setTitle("updatedTest1");

        Account account = new Account();
        account.setId(2);
        expected1.setAccount(account);
        expected1.setContent("This is my first #updated blog post EVER!");
        expected1.setAnchor("an updated Anchor1");
        Category mockCategory = new Category();
        mockCategory.setId(3);
        expected1.setCategory(mockCategory);

        Blog updated1 = blogDao.update(expected1);

        Assert.assertEquals(account.getId(), updated1.getAccount().getId());
        Assert.assertEquals(mockCategory.getId(), updated1.getCategory().getId());
        Assert.assertEquals("an updated Anchor1", updated1.getAnchor());
        Assert.assertEquals("This is my first #updated blog post EVER!", updated1.getContent());
        Assert.assertEquals("updatedTest1", updated1.getTitle());
        Assert.assertEquals(date.toString(), updated1.getStartDate().toString());
        Assert.assertEquals(dateEnd.toString(), updated1.getEndDate().toString());

    }
}
