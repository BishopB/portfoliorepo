/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DaoTests;

import com.thesoftwareguild.akronautobodycms.dao.StaticPageDao;
import com.thesoftwareguild.akronautobodycms.dao.StaticPageInf;
import com.thesoftwareguild.akronautobodycms.dto.Account;
import com.thesoftwareguild.akronautobodycms.dto.StaticPage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class StaticPageDaoTests {

    ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");

    StaticPageInf staticPageDao = ctx.getBean("staticDao", StaticPageInf.class);
    JdbcTemplate template = ctx.getBean("testJdbcTemplate", JdbcTemplate.class);

    StaticPage staticPage1 = new StaticPage();
    StaticPage staticPage2 = new StaticPage();
    StaticPage staticPage3 = new StaticPage();

    StaticPage expected2 = new StaticPage();
    StaticPage expected3 = new StaticPage();
    StaticPage expected1 = new StaticPage();

    Integer expected1Id = -1;
    Integer expected2Id = -1;
    Integer expected3Id = -1;

    public StaticPageDaoTests() {

    }

    @Before
    public void setUp() {
        Account account = new Account();
        account.setId(1);
        staticPage1.setAuthor(account);
        staticPage1.setContent("moop");
        staticPage1.setTitle("anoop");
        expected1 = staticPageDao.create(staticPage1);
        expected1Id = template.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

        Account account2 = new Account();
        account2.setId(2);
        staticPage2.setAuthor(account2);
        staticPage2.setContent("boop");
        staticPage2.setTitle("anoop");
        expected2 = staticPageDao.create(staticPage2);
        expected2Id = template.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

        Account account3 = new Account();
        account3.setId(3);
        staticPage3.setAuthor(account3);
        staticPage3.setContent("goop");
        staticPage3.setTitle("anoop");
        expected3 = staticPageDao.create(staticPage3);
        expected3Id = template.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

    }

    @After
    public void tearDown() {
        template.update("DELETE FROM `staticPage` WHERE staticPage_id =?", expected1Id);
        template.update("DELETE FROM `staticPage` WHERE staticPage_id =?", expected2Id);
        template.update("DELETE FROM `staticPage` WHERE staticPage_id =?", expected3Id);
    }

    @Test
    public void testCreateOneStaticPage() {
        Assert.assertTrue(!expected1.getId().toString().isEmpty());
        Assert.assertEquals(staticPage1, expected1);

    }

    @Test
    public void testCreateMultipleStaticPage() {
        Assert.assertTrue(!expected1.getId().toString().isEmpty());
        Assert.assertEquals(staticPage1, expected1);

        Assert.assertTrue(!expected2.getId().toString().isEmpty());
        Assert.assertEquals(staticPage2, expected2);

        Assert.assertTrue(!expected3.getId().toString().isEmpty());
        Assert.assertEquals(staticPage3, expected3);
    }

    @Test
    public void testReadOneStaticPage() {
        StaticPage readStaticPage1 = staticPageDao.read(expected1Id);

        Assert.assertTrue(readStaticPage1.getAuthor().getPassword().isEmpty());
        Assert.assertTrue(!readStaticPage1.getAuthor().getName().isEmpty());

        Assert.assertEquals(readStaticPage1.getTitle(), expected1.getTitle());
        Assert.assertEquals(readStaticPage1.getContent(), expected1.getContent());
        Assert.assertEquals(readStaticPage1.getId(), expected1.getId());

    }
    
    @Test
    public void testReadMultipleStaticPage() {
        StaticPage readStaticPage1 = staticPageDao.read(expected1Id);

        Assert.assertTrue(readStaticPage1.getAuthor().getPassword().isEmpty());
        Assert.assertTrue(!readStaticPage1.getAuthor().getName().isEmpty());

        Assert.assertEquals(readStaticPage1.getTitle(), expected1.getTitle());
        Assert.assertEquals(readStaticPage1.getContent(), expected1.getContent());
        Assert.assertEquals(readStaticPage1.getId(), expected1.getId());
        
        StaticPage readStaticPage2 = staticPageDao.read(expected2Id);

        Assert.assertTrue(readStaticPage2.getAuthor().getPassword().isEmpty());
        Assert.assertTrue(!readStaticPage2.getAuthor().getName().isEmpty());

        Assert.assertEquals(readStaticPage2.getTitle(), expected2.getTitle());
        Assert.assertEquals(readStaticPage2.getContent(), expected2.getContent());
        Assert.assertEquals(readStaticPage2.getId(), expected2.getId());
        
        StaticPage readStaticPage3 = staticPageDao.read(expected3Id);

        Assert.assertTrue(readStaticPage3.getAuthor().getPassword().isEmpty());
        Assert.assertTrue(!readStaticPage3.getAuthor().getName().isEmpty());

        Assert.assertEquals(readStaticPage3.getTitle(), expected3.getTitle());
        Assert.assertEquals(readStaticPage3.getContent(), expected3.getContent());
        Assert.assertEquals(readStaticPage3.getId(), expected3.getId());

    }
    
    @Test
    public void listStaticPages_Test_Size(){
//        Assert.assertEquals(3, staticPageDao.list().size());
        Assert.assertFalse(staticPageDao.list().isEmpty());
        Assert.assertFalse(staticPageDao.list().size()<3 || staticPageDao.list().size()>3);
       
    }
    
//    @Test
//    public void listStaticPages_attribute_Test(){
//        Assert.assertEquals(staticPageDao.list().get(0).getTitle(), expected1.getTitle());
//        Assert.assertEquals(staticPageDao.list().get(1).getTitle(), expected2.getTitle());
//        Assert.assertEquals(staticPageDao.list().get(2).getTitle(), expected3.getTitle());
//        
//        
//        Assert.assertEquals(staticPageDao.list().get(0).getContent(), expected1.getContent());
//        Assert.assertEquals(staticPageDao.list().get(1).getContent(), expected2.getContent());
//        Assert.assertEquals(staticPageDao.list().get(2).getContent(), expected3.getContent());
//        
//    }
}
