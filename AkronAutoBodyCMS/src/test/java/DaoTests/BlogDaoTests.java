/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DaoTests;

import com.thesoftwareguild.akronautobodycms.dao.BlogDaoInf;
import com.thesoftwareguild.akronautobodycms.dto.Account;
import com.thesoftwareguild.akronautobodycms.dto.Blog;
import com.thesoftwareguild.akronautobodycms.dto.Category;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
//import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class BlogDaoTests {

    ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");

    BlogDaoInf blogDao = ctx.getBean("blogDao", BlogDaoInf.class);
    JdbcTemplate template = ctx.getBean("testJdbcTemplate", JdbcTemplate.class);
    Blog target = new Blog();
    Blog target2 = new Blog();
    Blog target3 = new Blog();

    Integer expected1Id = -1;
    Integer expected2Id = -2;
    Integer expected3Id = -3;

    Blog expected1;
    Blog expected2;
    Blog expected3;
    List<Blog> testList = new ArrayList();

    public BlogDaoTests() {
    }

    @Before
    public void setUp() {

        LocalDate date = LocalDate.parse("2014-03-15");
        LocalDate dateEnd = LocalDate.parse("2020-07-11");
        target.setStartDate(date);
        target.setEndDate(dateEnd);

        List<String> hashtagList = new ArrayList();
        String hashtag = "hash1";
        hashtagList.add(hashtag);
        target.setHashtag(hashtagList);
        target.setTitle("newblogTest1");

        Account account = new Account();
        account.setId(1);
        target.setAccount(account);
        target.setContent("This is my first blog post EVER!#blog");
        target.setAnchor("an Anchor1");
        Category mockCategory = new Category();
        mockCategory.setId(1);
//        mockCategory.setCategory("test");
        target.setCategory(mockCategory);
        target.setApproved(true);
        expected1 = blogDao.create(target);
        expected1Id = template.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

        testList.add(target);

        LocalDate date2 = LocalDate.parse("2014-04-09");
        LocalDate dateEnd2 = LocalDate.parse("2019-06-11");

        target2.setStartDate(date2);
        target2.setEndDate(dateEnd2);
        List<String> hashtagList2 = new ArrayList();
        String hashtag2 = "hash2";
        hashtagList2.add(hashtag2);
        target2.setHashtag(hashtagList2);
        target2.setTitle("newblogTest2");

        Account account2 = new Account();
        account2.setId(2);
        target2.setAccount(account2);
        target2.setContent("This is my second #blog post EVER!");
        target2.setAnchor("an Anchor2");
        Category mockCategory2 = new Category();
        mockCategory2.setId(2);
        target2.setCategory(mockCategory2);
        target2.setApproved(false);
        expected2 = blogDao.create(target2);
        expected2Id = template.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        testList.add(target2);

        LocalDate date3 = LocalDate.parse("2014-04-09");
        LocalDate dateEnd3 = LocalDate.parse("2018-07-11");
        target3.setStartDate(date3);
        target3.setEndDate(dateEnd3);
        List<String> hashtagList3 = new ArrayList();
        String hashtag3 = "hash3";
        hashtagList3.add(hashtag3);
        target3.setHashtag(hashtagList3);
        target3.setTitle("newblogTest3");

        Account account3 = new Account();
        account3.setId(3);
        target3.setAccount(account3);
        target3.setContent("This is my third blog post EVER!");
        target3.setAnchor("an Anchor3");
        Category mockCategory3 = new Category();
        mockCategory3.setId(1);
        target3.setCategory(mockCategory3);
        target3.setApproved(true);
        expected3 = blogDao.create(target3);
        expected3Id = template.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);

        testList.add(target3);

    }

    @After
    public void tearDown() {
        template.update("DELETE FROM blog WHERE blog_id>0");

    }

    @Test
    public void createBlogTest1() {
//        BlogDao dao = ctx.getBean("blogDao");
        Assert.assertEquals(target, expected1);
        Assert.assertEquals(target2, expected2);
        Assert.assertEquals(target3, expected3);
        Assert.assertTrue(!expected1.getId().toString().isEmpty());
        Assert.assertTrue(!expected2.getId().toString().isEmpty());
        Assert.assertTrue(!expected3.getId().toString().isEmpty());
    }

    @Test
    public void listBlogsTest_For_Size() {

        Assert.assertEquals(2, blogDao.list().size());

    }

    @Test
    public void listBlogsTest_Attributes() {

        List<Blog> blogList = blogDao.list();
        Assert.assertTrue(blogList.get(0).getTitle().equals("newblogTest3"));
        Assert.assertTrue(blogList.get(1).getTitle().equals("newblogTest1"));

        Assert.assertTrue(blogList.get(0).getContent().equals("This is my third blog post EVER!"));
        Assert.assertTrue(blogList.get(1).getContent().equals("This is my first blog post EVER!#blog"));

    }

    @Test
    public void getBlogTest() {
        int expectedId = expected1.getId();
        Blog expectedBlog = blogDao.read(expectedId);
        Assert.assertEquals(expected1.getId(), expectedBlog.getId());
        Assert.assertEquals(expected1.getContent(), expectedBlog.getContent());
        Assert.assertEquals(expected1.getTitle(), expectedBlog.getTitle());

        int expectedId2 = expected2.getId();
        Blog expectedBlog2 = blogDao.read(expectedId2);
        Assert.assertEquals(expected2.getId(), expectedBlog2.getId());
        Assert.assertEquals(expected2.getContent(), expectedBlog2.getContent());
        Assert.assertEquals(expected2.getTitle(), expectedBlog2.getTitle());

        int expectedId3 = expected3.getId();
        Blog expectedBlog3 = blogDao.read(expectedId3);
        Assert.assertEquals(expected3.getId(), expectedBlog3.getId());
        Assert.assertEquals(expected3.getContent(), expectedBlog3.getContent());
        Assert.assertEquals(expected3.getTitle(), expectedBlog3.getTitle());
    }

    @Test
    public void listByHashTag_Size_Test() {
        Assert.assertEquals(1, blogDao.listByHashTag("#blog").size());
    }

    @Test
    public void listByHashTag_Attributes_Test() {
        String hashtag = "#blog";
        Assert.assertEquals(blogDao.listByHashTag(hashtag).get(0).getTitle(), expected1.getTitle());

        Assert.assertEquals(blogDao.listByHashTag(hashtag).get(0).getContent(), expected1.getContent());

        Assert.assertEquals(blogDao.listByHashTag(hashtag).get(0).getAnchor(), expected1.getAnchor());
    }

    @Test
    public void listByCategory_Size_Test() {
        Assert.assertEquals(2, blogDao.listByCategory("test").size());
    }

    @Test
    public void listByCategory_Attributes_Test() {
        Assert.assertTrue(blogDao.listByCategory("test").stream().anyMatch(blog -> blog.getTitle().equals(expected1.getTitle())));
        Assert.assertTrue(blogDao.listByCategory("test").stream().anyMatch(blog -> blog.getTitle().equals(expected3.getTitle())));

        Assert.assertTrue(blogDao.listByCategory("test").stream().anyMatch(blog -> blog.getContent().equals(expected1.getContent())));
        Assert.assertTrue(blogDao.listByCategory("test").stream().anyMatch(blog -> blog.getContent().equals(expected3.getContent())));

        Assert.assertTrue(blogDao.listByCategory("test").stream().anyMatch(blog -> blog.getAccount().getId().equals(expected1.getAccount().getId())));
        Assert.assertTrue(blogDao.listByCategory("test").stream().anyMatch(blog -> blog.getAccount().getId().equals(expected3.getAccount().getId())));

        Assert.assertTrue(blogDao.listByCategory("test").stream().anyMatch(blog -> blog.getCategory().getId().equals(expected1.getCategory().getId())));
        Assert.assertTrue(blogDao.listByCategory("test").stream().anyMatch(blog -> blog.getCategory().getId().equals(expected1.getCategory().getId())));

    }
    @Test
    public void listByAuthor_Size_Test() {
        Assert.assertEquals(1, blogDao.listByAuthor("test").size());
    }
    @Test
    public void listByAuthor_Attributes_Test(){
        Assert.assertEquals(blogDao.listByAuthor("test").get(0).getTitle(), expected1.getTitle());
        Assert.assertEquals(blogDao.listByAuthor("test").get(0).getContent(), expected1.getContent());
        Assert.assertEquals(blogDao.listByAuthor("test").get(0).getAnchor(), expected1.getAnchor());
        
    }
    @Test
    public void listByKeyWord_Size_Test() {
        Assert.assertEquals(1, blogDao.listByKeyword("third").size());
    }
    @Test
    public void listByKeyWord_Attributes_Test(){
        Assert.assertEquals(blogDao.listByKeyword("third").get(0).getTitle(), expected3.getTitle());
        Assert.assertEquals(blogDao.listByKeyword("third").get(0).getContent(), expected3.getContent());
        Assert.assertEquals(blogDao.listByKeyword("third").get(0).getAnchor(), expected3.getAnchor());
        
    }
}
